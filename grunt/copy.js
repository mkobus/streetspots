module.exports = {
    fontawesome_fonts: {
        'expand': true,
        cwd: 'node_modules/font-awesome/fonts',
        src: '**',
        dest: 'src/StreetSpots/WebsiteBundle/Resources/public/fonts',
        filter: 'isFile'
    }
};
