module.exports = {
    sass: {
        options: {
            title: 'Task Complete',
            message: 'SASS task finished running'
        }
    },
    js: {
        options: {
            title: 'Task Complete',
            message: 'JS task finished running'
        }
    },
    build: {
        options: {
            title: 'Task Complete',
            message: 'Assets built and installed'
        }
    }
};
