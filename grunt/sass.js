module.exports = {
    options: {
        includePaths: ['node_modules/'],
        sourceMap: true,
        sourceMapContents: true,
        debugInfo: true
    },
    dist: {
        options: {
            style: 'expanded'
        },
        files: {
            'src/StreetSpots/WebsiteBundle/Resources/public/css/app.css': 'src/StreetSpots/WebsiteBundle/Resources/scss/app.scss'
        }
    }
};
