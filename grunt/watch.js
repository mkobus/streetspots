module.exports = {
    grunt: { files: ['Gruntfile.js'] },
    sass: {
        files: ['src/StreetSpots/WebsiteBundle/Resources/scss/*.scss'],
        tasks: ['_buildcss']
    },
    js: {
        files: ['src/StreetSpots/WebsiteBundle/Resources/js/*.js'],
        tasks: ['_buildjs']
    }
};
