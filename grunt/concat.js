module.exports = {
    app: {
        src: [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/bootstrap/js/dist/util.js',
            'node_modules/bootstrap/js/dist/button.js',
            // 'node_modules/bootstrap/js/dist/dropdown.js',
            'node_modules/bootstrap/js/dist/carousel.js',
            'src/StreetSpots/WebsiteBundle/Resources/public/js/custom.js'
        ],
        dest: 'src/StreetSpots/WebsiteBundle/Resources/public/js/app.js'
    }
};
