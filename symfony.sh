#!/usr/bin/env bash

#
# use this script to call symfony CLI tool on remote vagrant server
#

DATA_WWW="/data/www"
SYMFONY_CMD=$*

# have to run shell with -l option to ensure .profile is loaded (it has some ENV variables)
vagrant ssh -- -t "sh -l -c \"cd ${DATA_WWW}; php bin/console ${SYMFONY_CMD}\""