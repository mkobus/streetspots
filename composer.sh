#!/usr/bin/env bash

#
# use this script to call composer on remote vagrant server
#

DATA_WWW="/data/www"
COMPOSER_CMD=$*

# have to run shell with -l option to ensure .profile is loaded (it has some ENV variables)
vagrant ssh -- -t "sh -l -c \"cd ${DATA_WWW}; composer ${COMPOSER_CMD}\""
