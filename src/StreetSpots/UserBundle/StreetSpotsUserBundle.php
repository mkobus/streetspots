<?php

namespace StreetSpots\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class StreetSpotsUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
