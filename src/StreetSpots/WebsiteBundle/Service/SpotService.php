<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 08.03.2017
 * Time: 11:25
 */

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Repository\SpotRepository;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Repository\UsersRelationshipRepository;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipAlreadyExistsException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipDoesNotExistException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipProhibited;

class SpotService extends ServiceBase
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var SpotRepository */
    protected $entityRepository;

    /** @var LocationService */
    protected $locationService;

    /**
     * SpotService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param SpotRepository         $spotRepository
     * @param LocationService        $locationService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        SpotRepository $spotRepository,
        LocationService $locationService
    ) {
        parent::__construct($entityManager, $spotRepository);
        $this->locationService = $locationService;
    }

    /**
     * @return int
     */
    public function getEntityTypeId()
    {
        return EntityTypeRepository::ENTITY_TYPE_SPOT;
    }

    /**
     * @param Tag $tag
     *
     * @return Spot[]
     */
    public function findByTag(Tag $tag)
    {
        return $this->entityRepository->findByTag($tag);
    }

    /**
     * @param Tag $tag
     *
     * @return Query
     */
    public function findByTagPaginated(Tag $tag)
    {
        return $this->entityRepository->findByTagQuery($tag);
    }

    /**
     * @param User $user
     *
     * @return Spot[]
     */
    public function findCreatedByUser(User $user)
    {
        return $this->entityRepository->findCreatedByUser($user);
    }

    /**
     * @param User $user
     *
     * @return Spot[]
     */
    public function findUpdatedByUser(User $user)
    {
        return $this->entityRepository->findUpdatedByUser($user);
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function findRatedByUser(User $user)
    {
        return $this->entityRepository->findRatedByUser($user);
    }

    /**
     * @param Spot  $spot
     * @param float $distance
     * @param int   $limit
     *
     * @return array [0 => Spot, 'distance' => float]
     */
    public function findInRange(Spot $spot, float $distance, int $limit = 15)
    {
        $latitude = $spot->getLocation()->getLatitude();
        $longitude = $spot->getLocation()->getLongitude();

        if ($latitude < -90 || $latitude > 90) {
            throw new \InvalidArgumentException('$latitude value has to be between -90 and 90');
        }

        if ($longitude < -180 || $longitude > 180) {
            throw new \InvalidArgumentException('$longitude value has to be between -90 and 90');
        }

        if ($distance < 0) {
            throw new \InvalidArgumentException('$distance cannot be lower than 0.');
        }

        $results = $this->entityRepository->findInRange($spot, $distance, $limit);


        return $results;
    }

    /**
     * @return Spot|EntityInterface
     */
    public function createEntity()
    {
        /** @var Spot $spot */
        $spot = parent::createEntity();
        $spot->setLocation($this->locationService->createEntity());

        return $spot;
    }
}
