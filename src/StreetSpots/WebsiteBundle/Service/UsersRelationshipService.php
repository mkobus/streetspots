<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 08.03.2017
 * Time: 11:25
 */

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Entity\UsersRelationship;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Repository\UsersRelationshipRepository;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipAlreadyExistsException;

class UsersRelationshipService
{
    /**
     * @param User $user
     * @param User $following
     *
     * @return bool
     */
    public function isUserFollowing(User $user, User $following)
    {
        return $user->getFollowing()->contains($following);
    }
}
