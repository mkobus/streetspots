<?php

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\Tagging;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Repository\TaggingRepository;
use StreetSpots\WebsiteBundle\Repository\TagRepository;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Repository\UsersRelationshipRepository;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipAlreadyExistsException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipDoesNotExistException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipProhibited;

class TaggingService extends ServiceBase
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var TagRepository */
    protected $entityRepository;

    /**
     * TagService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TaggingRepository      $taggingRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TaggingRepository $taggingRepository
    ) {
        parent::__construct($entityManager, $taggingRepository);
    }

    /**
     * @return int
     */
    public function getEntityTypeId()
    {
        return EntityTypeRepository::ENTITY_TYPE_TAGGING;
    }

    /**
     * @return Tagging|EntityInterface
     */
    public function createEntity()
    {
        return parent::createEntity();
    }
}
