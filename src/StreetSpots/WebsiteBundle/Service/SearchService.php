<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 08.03.2017
 * Time: 11:25
 */

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Repository\TagRepository;
use StreetSpots\WebsiteBundle\Repository\UserRepository;

class SearchService
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var UserRepository */
    protected $userRepository;

    /** @var TagRepository */
    protected $tagRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserRepository         $userRepository
     * @param TagRepository          $tagRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        TagRepository $tagRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param string $username
     * @param bool   $exact
     *
     * @return User[]
     */
    public function findUsersByUsername($username, $exact = false)
    {
        return $this->userRepository->findByUsername($username, $exact);
    }

    /**
     * @param string $username
     * @param bool   $exact
     *
     * @return Query
     */
    public function findUsersByUsernamePaginated($username, $exact = false)
    {
        return $this->userRepository->findByUsernameQuery($username, $exact);
    }

    /**
     * @param string $name
     * @param bool   $exact
     *
     * @return Tag[]
     */
    public function findTagsByName($name, $exact = false)
    {
        return $this->tagRepository->findByName($name, $exact);
    }

    /**
     * @param string $name
     * @param bool   $exact
     *
     * @return Query
     */
    public function findTagsByNamePaginated($name, $exact = false)
    {
        return $this->tagRepository->findByNameQuery($name, $exact);
    }
}
