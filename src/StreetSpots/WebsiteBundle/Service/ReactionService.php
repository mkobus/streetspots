<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 08.03.2017
 * Time: 11:25
 */

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use StreetSpots\WebsiteBundle\Entity\Behavior\Reactable;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\Reaction;
use StreetSpots\WebsiteBundle\Entity\ReactionType;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Repository\ReactionRepository;
use StreetSpots\WebsiteBundle\Repository\ReactionTypeRepository;
use StreetSpots\WebsiteBundle\Service\Exception\Reaction\UserAlreadyReactedException;

class ReactionService extends ServiceBase
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var ReactionRepository */
    protected $entityRepository;

    /**
     * UserService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ReactionRepository     $reactionRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ReactionRepository $reactionRepository
    ) {
        parent::__construct($entityManager, $reactionRepository);
    }

    /**
     * @return int
     */
    public function getEntityTypeId()
    {
        return EntityTypeRepository::ENTITY_TYPE_REACTION;
    }

    /**
     * @param User      $user
     * @param Reactable $entity
     *
     */
    public function like(User $user, Reactable $entity)
    {
        $this->react($user, $entity, ReactionTypeRepository::TYPE_LIKE);
    }

    /**
     * @param User      $user
     * @param Reactable $entity
     *
     */
    public function dislike(User $user, Reactable $entity)
    {
        $this->react($user, $entity, ReactionTypeRepository::TYPE_DISLIKE);
    }

    /**
     * @param User      $user
     * @param Reactable $entity
     * @param int       $type
     *
     * @throws UserAlreadyReactedException
     */
    private function react(User $user, Reactable $entity, $type)
    {
        /** @var Reaction|false $alreadyExistingReaction */
        $alreadyExistingReaction = $entity->getReactions()->filter(
            function (Reaction $reaction) use ($user) {
                return $reaction->getUser() === $user;
            }
        )->first();

        if (false !== $alreadyExistingReaction) {
            if ($type === $alreadyExistingReaction->getReactionType()->getId()) {
                throw new UserAlreadyReactedException($user, $entity, $type);
            }

            $entity->removeReaction($alreadyExistingReaction);
        }

        $reaction = $this->createEntity();
        $reaction->setReactionType($this->entityManager->getReference(ReactionType::class, $type));
        $reaction->setUser($user);

        $entity->addReaction($reaction);

        $this->entityManager->persist($reaction);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @param User      $user
     * @param Reactable $entity
     *
     * @return Reaction|null
     */
    public function getUserReaction(User $user, Reactable $entity)
    {
        $userReaction = $entity->getReactions()->filter(
            function (Reaction $reaction) use ($user) {
                return $reaction->getUser() === $user;
            }
        )->first();

        return $userReaction ?: null;
    }

    /**
     * @param User      $user
     * @param Reactable $entity
     *
     * @return bool
     */
    public function hasUserAlreadyLiked(User $user, Reactable $entity)
    {
        return $this->hasUserAlreadyReacted($user, $entity, ReactionTypeRepository::TYPE_LIKE);
    }

    /**
     * @param User      $user
     * @param Reactable $entity
     *
     * @return bool
     */
    public function hasUserAlreadyDisliked(User $user, Reactable $entity)
    {
        return $this->hasUserAlreadyReacted($user, $entity, ReactionTypeRepository::TYPE_DISLIKE);
    }

    /**
     * @param User      $user
     * @param Reactable $entity
     * @param int|null  $type
     *
     * @return bool
     */
    public function hasUserAlreadyReacted(User $user, Reactable $entity, $type = null)
    {
        return $entity->getReactions()->exists(
            function ($key, Reaction $reaction) use ($user, $type) {
                $checkType = null === $type ? true : $reaction->getReactionType()->getId() == $type;

                return $reaction->getUser() === $user && $checkType;
            }
        );
    }

    /**
     * @param Reactable $entity
     *
     * @return int
     */
    public function getLikeCount(Reactable $entity)
    {
        return $entity->getLikes()->count();
    }

    /**
     * @param Reactable $entity
     *
     * @return int
     */
    public function getDislikeCount(Reactable $entity)
    {
        return $entity->getDislikes()->count();
    }

    /**
     * @return Reaction|EntityInterface
     */
    public function createEntity()
    {
        return parent::createEntity();
    }
}
