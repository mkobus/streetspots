<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 08.03.2017
 * Time: 11:25
 */

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\SpotImage;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Repository\SpotImageRepository;
use StreetSpots\WebsiteBundle\Repository\SpotRepository;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Repository\UsersRelationshipRepository;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipAlreadyExistsException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipDoesNotExistException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipProhibited;

class SpotImageService extends ServiceBase
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var SpotImageRepository */
    protected $entityRepository;

    /**
     * SpotImageService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param SpotImageRepository    $spotImageRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        SpotImageRepository $spotImageRepository
    ) {
        parent::__construct($entityManager, $spotImageRepository);
    }

    /**
     * @return int
     */
    public function getEntityTypeId()
    {
        return EntityTypeRepository::ENTITY_TYPE_SPOT_IMAGE;
    }

    /**
     * @return SpotImage|EntityInterface
     */
    public function createEntity()
    {
        return parent::createEntity();
    }
}
