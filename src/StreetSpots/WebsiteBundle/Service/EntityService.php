<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 08.03.2017
 * Time: 11:25
 */

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\EntityType;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Service\Exception\Common\EntityNotFoundException;

class EntityService
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var EntityTypeRepository */
    protected $entityTypeRepository;

    /**
     * UserService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param EntityTypeRepository   $entityTypeRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        EntityTypeRepository $entityTypeRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entityTypeRepository = $entityTypeRepository;
    }

    /**
     * @param int $entityTypeId
     * @param int $entityId
     *
     * @return EntityInterface|null
     *
     * @throws EntityNotFoundException
     */
    public function getEntity($entityTypeId, $entityId)
    {
        /** @var EntityType $entityType */
        $entityType = $this->entityTypeRepository->find($entityTypeId);

        if (null === $entityType) {
            throw new EntityNotFoundException(EntityTypeRepository::ENTITY_TYPE_ENTITY_TYPE, $entityTypeId);
        }

        $targetEntityRepository = $this->entityManager->getRepository($entityType->getEntityClass());
        /** @var EntityInterface $targetEntity */
        $targetEntity = $targetEntityRepository->find($entityId);

        if (null === $targetEntity) {
            throw new EntityNotFoundException($entityTypeId, $entityId);
        }

        return $targetEntity;
    }
}
