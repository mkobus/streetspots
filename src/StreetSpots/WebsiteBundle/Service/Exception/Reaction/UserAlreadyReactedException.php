<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 19.03.2017
 * Time: 11:23
 */

namespace StreetSpots\WebsiteBundle\Service\Exception\Reaction;

use StreetSpots\WebsiteBundle\Entity\Behavior\Reactable;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\ReactionTypeRepository;

class UserAlreadyReactedException extends \Exception
{
    /**
     * @param User                      $user
     * @param Reactable|EntityInterface $entity
     * @param int                       $type
     */
    public function __construct(User $user, EntityInterface $entity, $type)
    {
        $verb = ReactionTypeRepository::TYPE_LIKE === $type ? 'liked' : 'disliked';

        parent::__construct(
            'User '
            .$user->getUsername()
            .' already '.$verb.' entity '
            .'['.$entity->getEntityType()->getName().'] '.$entity.' (#'.$entity->getId().')'
        );
    }
}
