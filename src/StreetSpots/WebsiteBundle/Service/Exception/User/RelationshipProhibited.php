<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 19.03.2017
 * Time: 11:23
 */

namespace StreetSpots\WebsiteBundle\Service\Exception\User;

use StreetSpots\WebsiteBundle\Entity\User;

class RelationshipProhibited extends \Exception
{
    /**
     * @param User $follower
     * @param User $following
     */
    public function __construct(User $follower, User $following)
    {
        parent::__construct(
            'User '
            .$follower
            .' cannot follow '
            .$following
        );
    }
}
