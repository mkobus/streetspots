<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 19.03.2017
 * Time: 11:23
 */

namespace StreetSpots\WebsiteBundle\Service\Exception\Common;

class EntityNotFoundException extends \Exception
{
    /**
     * EntityNotFoundException constructor.
     *
     * @param int $entityTypeId
     * @param int $entityId
     */
    public function __construct($entityTypeId, $entityId)
    {
        parent::__construct(
            'Entity ID: '
            .$entityId
            .' of a type ID: '
            .$entityTypeId
            .' not found.'
        );
    }
}
