<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 08.03.2017
 * Time: 11:25
 */

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Repository\UsersRelationshipRepository;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipAlreadyExistsException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipDoesNotExistException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipProhibited;

class UserService extends ServiceBase
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var UserRepository */
    protected $entityRepository;

    /** @var UsersRelationshipService */
    protected $usersRelationshipService;

    /**
     * UserService constructor.
     *
     * @param EntityManagerInterface   $entityManager
     * @param UserRepository           $userRepository
     * @param UsersRelationshipService $usersRelationshipService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        UsersRelationshipService $usersRelationshipService
    ) {
        parent::__construct($entityManager, $userRepository);

        $this->usersRelationshipService = $usersRelationshipService;
    }

    /**
     * @return int
     */
    public function getEntityTypeId()
    {
        return EntityTypeRepository::ENTITY_TYPE_USER;
    }

    /**
     * @param User $follower
     * @param User $following
     *
     * @throws RelationshipAlreadyExistsException
     * @throws RelationshipProhibited
     */
    public function follow(User $follower, User $following)
    {
        if ($follower === $following) {
            throw new RelationshipProhibited($follower, $following);
        }

        if ($this->usersRelationshipService->isUserFollowing($follower, $following)) {
            throw new RelationshipAlreadyExistsException($follower, $following);
        }

        $follower->addFollowing($following);

        $this->entityManager->persist($follower);
        $this->entityManager->flush();
    }

    /**
     * @param User $follower
     * @param User $following
     *
     * @throws RelationshipDoesNotExistException
     * @throws RelationshipProhibited
     */
    public function unfollow(User $follower, User $following)
    {
        if ($follower === $following) {
            throw new RelationshipProhibited($follower, $following);
        }

        if (!$this->usersRelationshipService->isUserFollowing($follower, $following)) {
            throw new RelationshipDoesNotExistException($follower, $following);
        }

        $follower->removeFollowing($following);

        $this->entityManager->persist($follower);
        $this->entityManager->flush();
    }

    /**
     * @return User|EntityInterface
     */
    public function createEntity()
    {
        return parent::createEntity();
    }

    /**
     * @inheritDoc
     *
     * @param User $entity
     */
    protected function setEntityDefaultValues($entity)
    {
        $entity->setRoles(['ROLE_USER']);
    }
}
