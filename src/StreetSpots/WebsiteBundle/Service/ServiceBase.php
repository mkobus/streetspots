<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 06.10.2016
 * Time: 17:57
 */

namespace StreetSpots\WebsiteBundle\Service;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\EntityType;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;

abstract class ServiceBase
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var EntityRepository */
    protected $entityRepository;

    public function __construct(EntityManagerInterface $entityManager, EntityRepository $entityRepository)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $entityRepository;
    }

    /**
     * @return EntityInterface
     */
    public function createEntity()
    {
        $repository = $this->entityManager->getRepository(EntityType::class);

        /** @var EntityType $entityType */
        $entityType = $repository->find($this->getEntityTypeId());

        $className = $entityType->getEntityClass();

        /** @var EntityInterface $entity */
        $entity = new $className();
        $entity->setEntityType($entityType);
        $this->setEntityDefaultValues($entity);

        return $entity;
    }

    /**
     * @param EntityInterface $entity
     */
    protected function setEntityDefaultValues($entity)
    {
    }

    /**
     * @return int
     */
    abstract public function getEntityTypeId();
}
