<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 08.03.2017
 * Time: 11:25
 */

namespace StreetSpots\WebsiteBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\Location;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\EntityTypeRepository;
use StreetSpots\WebsiteBundle\Repository\LocationRepository;
use StreetSpots\WebsiteBundle\Repository\SpotRepository;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Repository\UsersRelationshipRepository;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipAlreadyExistsException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipDoesNotExistException;
use StreetSpots\WebsiteBundle\Service\Exception\User\RelationshipProhibited;

class LocationService extends ServiceBase
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var LocationRepository */
    protected $entityRepository;

    /**
     * LocationService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param LocationRepository     $locationRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LocationRepository $locationRepository
    ) {
        parent::__construct($entityManager, $locationRepository);
    }

    /**
     * @return int
     */
    public function getEntityTypeId()
    {
        return EntityTypeRepository::ENTITY_TYPE_LOCATION;
    }

    /**
     * @return Location|EntityInterface
     */
    public function createEntity()
    {
        return parent::createEntity();
    }
}
