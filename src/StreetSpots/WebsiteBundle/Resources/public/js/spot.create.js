var map, infoWindow, locationMarker, geocoderService;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 52.173932, lng: 19.456787},
        zoom: 5
    });
    infoWindow = new google.maps.InfoWindow;
    locationMarker = new google.maps.Marker({map: map});
    geocoderService = new google.maps.Geocoder;

    getLocation();
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(updatePosition, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function updatePosition(position) {
    let pos = {lat: position.coords.latitude, lng: position.coords.longitude};

    $('#spot_location_latitude').val(pos.lat);
    $('#spot_location_longitude').val(pos.lng);

    geocoderService.geocode({'location': pos}, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                $('#spot_location_name').val(results[0].formatted_address);
            } else {
                window.alert('No results found');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });

    locationMarker.setPosition(pos);
    map.setCenter(pos);
    map.setZoom(17);
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function addCollectionEntry(event) {
    const $parent = jQuery(this).parents('.card-group'),
        prototype = $parent.next('template').html();

    event.preventDefault();
    event.stopPropagation();

    if (prototype) {
        const indexPattern = new RegExp('__name__', 'g'),
            index = $parent.children('.card:not(.btn--collection-wrapper)').length,
            $newEntry = jQuery(prototype.replace(indexPattern, index)),
            $inputFile = $newEntry.find('input[type=file]'),
            $removeButton = $newEntry.find('.btn--collection-remove-entry');

        $parent.find('.btn--collection-wrapper').before($newEntry);
        $inputFile.on('change', handleFileSelect);
        $removeButton.on('click', removeCollectionEntry);
        $inputFile.trigger('click');
    }
}

function removeCollectionEntry(event) {
    const $parent = jQuery(this).parents('.card');

    $parent.remove();
}

function handleFileSelect(event) {
    const f = this.files[0],
        elementEvent = event,
        fileReader = new FileReader();

    fileReader.onload = function (event) {
        const imageUrl = event.target.result,
            $previewImage = jQuery(elementEvent.target).parents('.card').find('.card-img-top');

        $previewImage.attr('src', imageUrl);
    };

    fileReader.readAsDataURL(f);
}

jQuery(document).ready(function () {
    jQuery('#spot_location_get_position').on('click', getLocation);
    // jQuery('#spot_images').find('input[type="file"]').on('change', handleFileSelect);
    jQuery('.btn--collection-add-entry').on('click', addCollectionEntry);
});
