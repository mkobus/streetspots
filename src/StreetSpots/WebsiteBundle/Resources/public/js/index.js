var map, infoWindow, geocoderService;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 52.173932, lng: 19.456787},
        zoom: 17
    });
    infoWindow = new google.maps.InfoWindow;
    geocoderService = new google.maps.Geocoder;

    locations.forEach(function (element) {
        addMarkerLocation(element.latitude, element.longitude, element.type, element.url);
    });
}

function addMarkerLocation(lat, lng, type, url) {
    const position = {lat: lat, lng: lng},
        locationMarker = new google.maps.Marker({'map': map, 'url': url});

    locationMarker.setPosition(position);
    if (type === 'main') {
        map.setCenter(position);
    }

    google.maps.event.addListener(locationMarker, 'click', loadURL(locationMarker));
}

function loadURL(marker) {
    return function () {
        window.location.href = marker.url;
    }
}
