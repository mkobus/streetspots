<?php

namespace StreetSpots\WebsiteBundle\Form\Type;

use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Form\LocationType;
use StreetSpots\WebsiteBundle\Form\SpotImageType;
use StreetSpots\WebsiteBundle\StreetSpotsWebsiteBundle;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SpotType extends AbstractType
{
    public function getName()
    {
        return 'streetspots.type.spot';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'Name',
                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label' => 'Description',
                ]
            )
            ->add(
                'location',
                LocationType::class,
                [
                    'label' => 'Location',
                ]
            )
            ->add(
                'images',
                CollectionType::class,
                [
                    'label'        => 'Images',
                    'entry_type'   => SpotImageType::class,
                    'by_reference' => false,
                    'allow_add'    => true,
                    'allow_delete' => true,
                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'Create',
                ]
            );
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Spot::class,
            ]
        );
    }
}
