<?php

namespace StreetSpots\WebsiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('query', TextType::class)
            ->add(
                'type',
                ChoiceType::class,
                [
                    'expanded' => true,
                    'multiple' => false,
                    'choices'  => [
                        'User' => 'user',
                        'Tag'  => 'tag',
                    ],
                ]
            )
            ->add('submit', SubmitType::class, ['attr' => ['class' => 'btn-block btn-primary']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('placeholders', true);
        $resolver->setDefault('label', false);
    }

    public function getBlockPrefix()
    {
        return 'streetspots_search';
    }
}
