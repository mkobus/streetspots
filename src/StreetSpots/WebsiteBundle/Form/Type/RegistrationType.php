<?php
namespace StreetSpots\WebsiteBundle\Form\Type;

use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\VarDumper\VarDumper;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var FormBuilderInterface $formItem */
        foreach ($builder->all() as $formItem) {
            $itemOptions = $formItem->getOptions();
            $name = $formItem->getName();
            $class = get_class($formItem->getType()->getInnerType());

            $builder->remove($formItem->getName());

            $alteredOptions = [
                'attr'       => array_merge($itemOptions['attr'], ['placeholder' => $itemOptions['label']]),
                'label_attr' => array_merge(
                    $itemOptions['label_attr'],
                    [
                        'class' => 'sr-only',
                    ]
                ),
            ];

            if ($class === RepeatedType::class) {
                $firstChildLabel = $formItem->get('first')->getOption('label');
                $secondChildLabel = $formItem->get('second')->getOption('label');

                $alteredOptions += [
                    'first_options'  => [
                        'label'      => $firstChildLabel,
                        'label_attr' => [
                            'class' => 'sr-only',
                        ],
                        'attr'       => [
                            'placeholder' => $firstChildLabel,
                        ],
                    ],
                    'second_options' => [
                        'label'      => $secondChildLabel,
                        'label_attr' => [
                            'class' => 'sr-only',
                        ],
                        'attr'       => [
                            'placeholder' => $secondChildLabel,
                        ],
                    ],
                ];
            }

            $builder->add($name, $class, array_merge($itemOptions, $alteredOptions));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('placeholders', true);
        $resolver->setDefault('label', false);
    }

    public function getParent()
    {
        return RegistrationFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}
