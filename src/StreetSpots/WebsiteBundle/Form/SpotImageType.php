<?php

namespace StreetSpots\WebsiteBundle\Form;

use StreetSpots\WebsiteBundle\Entity\SpotImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SpotImageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'imageFile',
                VichImageType::class,
                [
                    'by_reference'  => false,
                    'property_path' => 'imageFile',
                    'required'      => false,
                    'allow_delete'  => false,
                    'download_link' => false,
                    'image_uri'     => false,
                    'attr'          => [
                        'accept'  => 'image/*',
                        'capture' => 'camera',
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SpotImage::class,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'streetspots_websitebundle_spotimage';
    }
}
