<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 09.08.2017
 * Time: 10:46
 */

namespace StreetSpots\WebsiteBundle\Repository;


use Doctrine\ORM\Query;
use DoctrineExtensions\Taggable\Entity\TagRepository as BaseTagRepository;
use StreetSpots\WebsiteBundle\Entity\Tag;

class TagRepository extends BaseTagRepository
{
    /**
     * @param string $name
     * @param bool   $exact
     *
     * @return Tag[]
     */
    public function findByName($name, $exact = false)
    {
        return $this->findByNameQuery($name, $exact)->getResult();
    }

    /**
     * @param string $name
     * @param bool   $exact
     *
     * @return Query
     */
    public function findByNameQuery($name, $exact)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $predicate = $exact
            ? $queryBuilder->expr()->eq('t.name', ':name')
            : $queryBuilder->expr()->like('t.name', ':name');
        $nameParameter = $exact
            ? $name
            : '%'.$name.'%';

        $query = $queryBuilder
            ->select('t')
            ->from('StreetSpotsWebsiteBundle:Tag', 't')
            ->where($predicate)
            ->setParameter('name', $nameParameter)
            ->getQuery();

        return $query;
    }
}
