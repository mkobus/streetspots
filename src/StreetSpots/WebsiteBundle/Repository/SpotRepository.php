<?php

namespace StreetSpots\WebsiteBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\QueryBuilder;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\User;

class SpotRepository extends EntityRepository
{
    /**
     * @param Tag $tag
     *
     * @return Spot[]
     */
    public function findByTag(Tag $tag)
    {
        return $this->findByTagQuery($tag)->getResult();
    }

    /**
     * @param Tag $tag
     *
     * @return Query
     */
    public function findByTagQuery(Tag $tag)
    {
        $tagRepository = $this->getEntityManager()->getRepository(Tag::class);

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $query = $queryBuilder
            ->select('s')
            ->from('StreetSpotsWebsiteBundle:Spot', 's')
            ->where('s.id IN (:ids)')
            ->getQuery()
            ->setParameter('ids', $tagRepository->getResourceIdsForTag(Spot::class, $tag->getName()));

        return $query;
    }

    /**
     * @param User $user
     *
     * @return Spot[]
     */
    public function findCreatedByUser(User $user)
    {
        return $this->findBy(
            [
                'createdBy' => $user,
            ]
        );
    }

    /**
     * @param User $user
     *
     * @return Spot[]
     */
    public function findRatedByUser(User $user)
    {
        $query = $this->createQueryBuilder('s')
            ->addSelect('r')
            ->innerJoin('s.ratings', 'r')
            ->where('r.user = :id')
            ->getQuery();

        $query->setParameter('id', $user->getId());

        return $query->getResult();
    }

    /**
     * @param User $user
     *
     * @return Spot[]
     */
    public function findUpdatedByUser(User $user)
    {
        return $this->findBy(
            [
                'updatedBy' => $user,
            ]
        );
    }

    /**
     * @param Spot  $spot
     * @param float $distance
     * @param int   $limit
     *
     * @return array [0 => Spot, 'distance' => float]
     */
    public function findInRange(Spot $spot, float $distance, int $limit)
    {
        $latitude = $spot->getLocation()->getLatitude();
        $longitude = $spot->getLocation()->getLongitude();

        $sqlQuery = '
            SELECT %s, ld.distance AS distance
            FROM (
                  SELECT id, distance 
                  FROM (
                        SELECT
                           z.id,
                           z.latitude,
                           z.longitude,
                           p.radius,
                           p.distance_unit * DEGREES(ACOS(COS(RADIANS(p.latpoint)) * COS(RADIANS(z.latitude)) * COS(RADIANS(p.longpoint - z.longitude)) + SIN(RADIANS(p.latpoint)) * SIN(RADIANS(z.latitude)))) AS distance 
                        FROM
                           location AS z 
                           JOIN (
                                 SELECT
                                    (:latitude) AS latpoint,
                                    (:longitude) AS longpoint,
                                    (:radius) AS radius,
                                    (:distanceUnit) AS distance_unit 
                           ) AS p ON 1 = 1 
                        WHERE
                           z.latitude BETWEEN p.latpoint - (p.radius / p.distance_unit) AND p.latpoint + (p.radius / p.distance_unit) 
                           AND z.longitude BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint)))) AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint)))) 
                  ) AS d 
                  WHERE distance <= radius
                  ORDER BY distance ASC
                  LIMIT :limit 
            ) AS ld 
            INNER JOIN spot s ON s.location_id = ld.id
            WHERE s.id != (:spotId)
        ';
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata('StreetSpotsWebsiteBundle:Spot', 's');
        $rsm->addScalarResult('distance', 'distance', 'string');

        $spotSelectClause = $rsm->generateSelectClause(['u' => 's']);

        $query = $this->getEntityManager()
            ->createNativeQuery(sprintf($sqlQuery, $spotSelectClause), $rsm)
            ->setParameter('latitude', $latitude)
            ->setParameter('longitude', $longitude)
            ->setParameter('radius', $distance)
            ->setParameter('distanceUnit', 111.045)
            ->setParameter('limit', $limit)
            ->setParameter('spotId', $spot->getId());

        return $query->getResult();
    }
}
