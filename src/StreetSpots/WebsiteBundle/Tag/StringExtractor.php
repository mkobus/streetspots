<?php

namespace StreetSpots\WebsiteBundle\Tag;

use Twitter_Extractor;

class StringExtractor extends Twitter_Extractor
{
    /**
     * @param null|string $text
     *
     * @return array
     */
    public function extractHashtags($text = null)
    {
        return parent::extractHashtags($text);
    }
}
