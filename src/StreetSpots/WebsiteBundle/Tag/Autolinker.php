<?php

namespace StreetSpots\WebsiteBundle\Tag;

use StreetSpots\WebsiteBundle\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Template;
use Twitter_Autolink;

class Autolinker extends Twitter_Autolink
{
    /** @var TagManager */
    protected $tagManager;

    /** @var RouterInterface */
    protected $router;

    /** @var EngineInterface */
    protected $templating;

    /**
     * @param TagManager      $tagManager
     * @param RouterInterface $router
     * @param EngineInterface $templating
     */
    public function __construct(TagManager $tagManager, RouterInterface $router, EngineInterface $templating)
    {
        parent::__construct();

        $this->tagManager = $tagManager;
        $this->router = $router;
        $this->templating = $templating;
    }

    public function autolinkTags(string $text)
    {
        $tags = $this->tagManager->loadTags(
            $this->extractor->extractHashtags($text)
        );
        $tagNamesInOrder = array_map(
            function (Tag $tag) {
                return $tag->getName();
            },
            $tags
        );
        array_multisort(array_map('strlen', $tagNamesInOrder), $tags, $tagNamesInOrder);
        $tags = array_reverse($tags);
        $tagNamesInOrder = array_reverse($tagNamesInOrder);
        $hashTags = $this->appendHash($tagNamesInOrder);
        $links = $this->renderLinksForTags($tags);

        return str_replace($hashTags, $links, $text);
    }

    /**
     * @param $tagNames
     *
     * @return array
     */
    private function appendHash($tagNames): array
    {
        return array_map(
            function (string $string) {
                return '#'.$string;
            },
            $tagNames
        );
    }

    /**
     * @param $tags
     *
     * @return array
     */
    private function renderLinksForTags($tags): array
    {
        return array_map(
            function (Tag $tag) {
                return $this->templating->render('@StreetSpotsWebsite/Partials/tag_link.html.twig', ['tag' => $tag]);
            },
            $tags
        );
    }
}
