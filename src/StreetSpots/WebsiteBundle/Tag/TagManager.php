<?php

namespace StreetSpots\WebsiteBundle\Tag;

use Doctrine\ORM\EntityManager;
use DoctrineExtensions\Taggable\Entity\Tag;
use DoctrineExtensions\Taggable\Taggable;
use DoctrineExtensions\Taggable\TagManager as BaseTagManager;
use FPN\TagBundle\Util\SlugifierInterface;
use StreetSpots\WebsiteBundle\Service\TaggingService;
use StreetSpots\WebsiteBundle\Service\TagService;

class TagManager extends BaseTagManager
{
    /** @var TagService */
    protected $tagService;

    /** @var TaggingService */
    protected $taggingService;

    /** @var  SlugifierInterface */
    protected $slugifier;

    /**
     * TagManager constructor.
     *
     * @param EntityManager      $entityManager
     * @param TagService         $tagService
     * @param TaggingService     $taggingService
     * @param SlugifierInterface $slugifier
     * @param null|string        $tagClass
     * @param null|string        $taggingClass
     */
    public function __construct(EntityManager $entityManager, TagService $tagService, TaggingService $taggingService, SlugifierInterface $slugifier, $tagClass = null, $taggingClass = null)
    {
        parent::__construct($entityManager, $tagClass, $taggingClass);

        $this->slugifier = $slugifier;
        $this->tagService = $tagService;
        $this->taggingService = $taggingService;
    }

    /**
     * @inheritDoc
     */
    protected function createTag($name)
    {
        $tag = $this->tagService->createEntity();
        $tag->setName($name);
        $tag->setSlug($this->slugifier->slugify($name));

        return $tag;
    }

    /**
     * @inheritDoc
     */
    protected function createTagging(Tag $tag, Taggable $resource)
    {
        $tagging = $this->taggingService->createEntity();
        $tagging->setTag($tag);
        $tagging->setResource($resource);

        return $tagging;
    }

    /**
     * Loads multiples tags from a list of tag names
     *
     * @param array $names Array of tag names
     *
     * @return Tag[]
     */
    public function loadTags(array $names)
    {
        if (empty($names)) {
            return [];
        }

        $names = array_unique($names);

        $builder = $this->em->createQueryBuilder();

        $tags = $builder
            ->select('t')
            ->from($this->tagClass, 't')
            ->where($builder->expr()->in('t.name', $names))
            ->getQuery()
            ->getResult();

        $loadedNames = [];
        foreach ($tags as $tag) {
            $loadedNames[] = $tag->getName();
        }

        return $tags;
    }

}
