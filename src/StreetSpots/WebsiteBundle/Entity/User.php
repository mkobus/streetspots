<?php

namespace StreetSpots\WebsiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use StreetSpots\WebsiteBundle\Entity\Behavior\ReactableTrait;
use StreetSpots\WebsiteBundle\Validator\Constraints as StreetSpotsAssert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 * @ORM\Table(name="users", indexes={@ORM\Index(columns={"username"})})
 * @ORM\Entity(repositoryClass="StreetSpots\WebsiteBundle\Repository\UserRepository")
 */
class User extends BaseUser implements EntityInterface
{
    use EntityTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=160, nullable=true)
     *
     * @StreetSpotsAssert\LinesNumber(lines=3)
     */
    private $bio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @Assert\Url()
     */
    private $website;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Assert\File(
     *     maxSize="200k",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg", "image/gif"}
     * )
     * @Vich\UploadableField(mapping="user_picture", fileNameProperty="imageName")
     *
     * @Assert\Image(
     *     allowSquare=true,
     *     allowPortrait=false,
     *     allowLandscape=false,
     *     minWidth="200",
     *     minHeight="200",
     *     maxWidth="1000",
     *     maxHeight="1000",
     *     detectCorrupted=true
     * );
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @var User[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="following", cascade={"persist", "remove"})
     **/
    private $followers;

    /**
     * @var User[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="followers", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="followers",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="following_user_id", referencedColumnName="id")}
     * )
     */
    private $following;

    /**
     * Not domain-related, kept for easier data import.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $yelpId;

    /**
     * Not domain-related, kept for easier data import.
     *
     * @ORM\Column(type="array", nullable=true)
     *
     * @var string[]
     */
    private $yelpFriends;

    /**
     * @var Spot[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="StreetSpots\WebsiteBundle\Entity\Spot", mappedBy="createdBy")
     * @ORM\JoinColumn(referencedColumnName="created_by")
     */
    private $spots;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param mixed $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param mixed $bio
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return User
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Add follower.
     *
     * @param User $follower
     *
     * @return User
     */
    public function addFollower(User $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower.
     *
     * @param User $follower
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFollower(User $follower)
    {
        return $this->followers->removeElement($follower);
    }

    /**
     * @param ArrayCollection|User[] $followers
     */
    public function setFollowers($followers)
    {
        $this->followers = $followers;
    }

    /**
     * Get followers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Add following.
     *
     * @param User $following
     *
     * @return User
     */
    public function addFollowing(User $following)
    {
        $this->following[] = $following;

        return $this;
    }

    /**
     * Remove following.
     *
     * @param User $following
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFollowing(User $following)
    {
        return $this->following->removeElement($following);
    }

    /**
     * @param ArrayCollection|User[] $following
     */
    public function setFollowing($following)
    {
        $this->following = $following;
    }

    /**
     * Get following.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowing()
    {
        return $this->following;
    }

    /**
     * @return ArrayCollection|Spot[]
     */
    public function getSpots()
    {
        return $this->spots;
    }

    /**
     * @param ArrayCollection|Spot[] $spots
     */
    public function setSpots($spots)
    {
        $this->spots = $spots;
    }


    /**
     * @param Spot $spot
     *
     * @return User
     */
    public function addSpot(Spot $spot)
    {
        $this->spots[] = $spot;

        return $this;
    }

    /**
     * @param Spot $spot
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSpot(Spot $spot)
    {
        return $this->spots->removeElement($spot);
    }

    /**
     * @return string
     */
    public function getYelpId(): string
    {
        return $this->yelpId;
    }

    /**
     * @param string $yelpId
     */
    public function setYelpId(string $yelpId)
    {
        $this->yelpId = $yelpId;
    }

    /**
     * @return \string[]
     */
    public function getYelpFriends(): array
    {
        return $this->yelpFriends;
    }

    /**
     * @param \string[] $yelpFriends
     */
    public function setYelpFriends(array $yelpFriends)
    {
        $this->yelpFriends = $yelpFriends;
    }


    /**
     * @return string
     */
    function __toString()
    {
        return $this->getUsername();
    }
}
