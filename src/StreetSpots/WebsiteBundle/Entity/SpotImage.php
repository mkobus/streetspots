<?php

namespace StreetSpots\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Blameable;
use StreetSpots\WebsiteBundle\Entity\Behavior\BlameableTrait;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SpotImage
 *
 * @Vich\Uploadable
 * @ORM\Table(name="spot_image")
 * @ORM\Entity(repositoryClass="StreetSpots\WebsiteBundle\Repository\SpotImageRepository")
 */
class SpotImage implements EntityInterface, Blameable
{
    use EntityTrait;
    use BlameableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="spot_image", fileNameProperty="imageName");
     * @Assert\File(
     *     maxSize="500k",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg", "image/gif"}
     * );
     * @Assert\Image(
     *     allowSquare=true,
     *     allowPortrait=false,
     *     allowLandscape=false,
     *     minWidth="200",
     *     minHeight="200",
     *     maxWidth="1000",
     *     maxHeight="1000",
     *     detectCorrupted=true
     * );
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @var Spot
     *
     * @ORM\ManyToOne(targetEntity="Spot", inversedBy="images", cascade={"persist"})
     */
    private $spot;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return User
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @return Spot
     */
    public function getSpot(): Spot
    {
        return $this->spot;
    }

    /**
     * @param Spot $spot
     */
    public function setSpot(Spot $spot)
    {
        $this->spot = $spot;
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return $this->getImageName();
    }
}
