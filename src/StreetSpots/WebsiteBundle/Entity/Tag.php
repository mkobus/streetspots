<?php

namespace StreetSpots\WebsiteBundle\Entity;

use FPN\TagBundle\Entity\Tag as BaseTag;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tag", indexes={@ORM\Index(columns={"name", "slug"})})
 * @ORM\Entity(repositoryClass="StreetSpots\WebsiteBundle\Repository\TagRepository")
 */
class Tag extends BaseTag
{
    use EntityTrait;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Tagging", mappedBy="tag", fetch="EAGER")
     **/
    protected $tagging;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTagging()
    {
        return $this->tagging;
    }

    /**
     * @param mixed $tagging
     */
    public function setTagging($tagging)
    {
        $this->tagging = $tagging;
    }
}
