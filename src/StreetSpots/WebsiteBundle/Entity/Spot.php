<?php

namespace StreetSpots\WebsiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Sluggable;
use StreetSpots\WebsiteBundle\Entity\Behavior\BlameableTrait;
use StreetSpots\WebsiteBundle\Entity\Behavior\Reactable;
use StreetSpots\WebsiteBundle\Entity\Behavior\ReactableTrait;
use StreetSpots\WebsiteBundle\Entity\Behavior\Taggable;
use StreetSpots\WebsiteBundle\Entity\Behavior\TaggableTrait;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Spot
 *
 * @ORM\Table(name="spot", indexes={@ORM\Index(columns={"slug"})})
 * @ORM\Entity(repositoryClass="StreetSpots\WebsiteBundle\Repository\SpotRepository")
 */
class Spot implements Taggable, Sluggable, EntityInterface, Reactable
{
    use EntityTrait;
    use TaggableTrait;
    use ReactableTrait;
    use BlameableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=true, unique=true)
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var Location
     *
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="spots", cascade={"persist"})
     */
    private $location;

    /**
     * @var SpotImage[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="SpotImage", mappedBy="spot", cascade={"persist", "remove"})
     */
    private $images;

    /**
     * Not domain-related, kept for easier data import.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $yelpId;

    /**
     * @var Rating[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="StreetSpots\WebsiteBundle\Entity\Rating", cascade={"persist", "remove"})
     */
    protected $ratings;

    /**
     * Spot constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    /**
     * @return ArrayCollection|SpotImage[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection|SpotImage[] $images
     */
    public function setImages(ArrayCollection $images)
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getYelpId(): string
    {
        return $this->yelpId;
    }

    /**
     * @param string $yelpId
     */
    public function setYelpId(string $yelpId)
    {
        $this->yelpId = $yelpId;
    }

    /**
     * @return ArrayCollection|Rating[]
     */
    public function getRatings()
    {
        return $this->ratings;
    }

    /**
     * @param ArrayCollection|Rating[] $ratings
     */
    public function setRatings($ratings)
    {
        $this->ratings = $ratings;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->getName();
    }
}
