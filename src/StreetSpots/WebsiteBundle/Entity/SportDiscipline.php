<?php

namespace StreetSpots\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use StreetSpots\WebsiteBundle\Entity\Behavior\Reactable;
use StreetSpots\WebsiteBundle\Entity\Behavior\ReactableTrait;

/**
 * SportDiscipline
 *
 * @ORM\Table(name="sport_discipline")
 * @ORM\Entity(repositoryClass="StreetSpots\WebsiteBundle\Repository\SportDisciplineRepository")
 */
class SportDiscipline implements EntityInterface
{
    use EntityTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SportDiscipline
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->getName();
    }
}
