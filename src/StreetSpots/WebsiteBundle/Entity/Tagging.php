<?php

namespace StreetSpots\WebsiteBundle\Entity;

use FPN\TagBundle\Entity\Tagging as BaseTagging;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tagging", uniqueConstraints={@ORM\UniqueConstraint(name="tagging_idx", columns={"tag_id", "resource_type", "resource_id"})})
 * @ORM\Entity(repositoryClass="StreetSpots\WebsiteBundle\Repository\TaggingRepository")
 */
class Tagging extends BaseTagging implements EntityInterface
{
    use EntityTrait;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Tag", inversedBy="tagging")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     **/
    protected $tag;

    /**
     * @param mixed $resourceType
     */
    public function setResourceType($resourceType)
    {
        $this->resourceType = $resourceType;
    }

    /**
     * @param mixed $resourceId
     */
    public function setResourceId($resourceId)
    {
        $this->resourceId = $resourceId;
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return $this->getTag()->getName().' -> '.$this->getResourceType().' ('.$this->getResourceId().')';
    }

}
