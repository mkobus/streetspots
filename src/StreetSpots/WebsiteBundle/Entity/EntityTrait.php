<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 17.10.2016
 * Time: 20:48
 */

namespace StreetSpots\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

trait EntityTrait
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var EntityType
     *
     * @ORM\ManyToOne(targetEntity="EntityType", cascade={"persist"})
     */
    protected $entityType;

    /**
     * Set entityTypeId
     *
     * @param EntityType $entityType
     *
     * @return static
     *
     */
    public function setEntityType(EntityType $entityType)
    {
        $this->entityType = $entityType;

        return $this;
    }

    /**
     * Get entityType
     *
     * @return EntityType
     */
    public function getEntityType()
    {
        return $this->entityType;
    }
}
