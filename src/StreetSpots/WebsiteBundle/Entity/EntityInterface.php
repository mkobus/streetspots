<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 17.10.2016
 * Time: 20:24
 */

namespace StreetSpots\WebsiteBundle\Entity;


interface EntityInterface
{
    /**
     * Set entityType
     *
     * @param EntityType $entityType
     *
     * @return self
     */
    public function setEntityType(EntityType $entityType);

    /**
     * Get entityType
     *
     * @return EntityType
     */
    public function getEntityType();

    /**
     * Get string representation of an entity
     *
     * @return string
     */
    public function __toString();
}
