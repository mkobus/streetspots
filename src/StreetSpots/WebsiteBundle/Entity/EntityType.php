<?php

namespace StreetSpots\WebsiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EntityType
 *
 * @ORM\Table(name="entity_type")
 * @ORM\Entity(repositoryClass="StreetSpots\WebsiteBundle\Repository\EntityTypeRepository")
 */
class EntityType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_class", type="string", length=255)
     */
    private $entityClass;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_repository_class", type="string", length=255)
     */
    private $entityRepositoryClass;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EntityType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set entityClass.
     *
     * @param string $entityClass
     *
     * @return EntityType
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * Get entityClass.
     *
     * @return string
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * Set entityRepositoryClass.
     *
     * @param string $entityRepositoryClass
     *
     * @return EntityType
     */
    public function setEntityRepositoryClass($entityRepositoryClass)
    {
        $this->entityRepositoryClass = $entityRepositoryClass;

        return $this;
    }

    /**
     * Get entityRepositoryClass.
     *
     * @return string
     */
    public function getEntityRepositoryClass()
    {
        return $this->entityRepositoryClass;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reactions = new ArrayCollection();
    }

    /**
     * Remove reaction.
     *
     * @param Reaction $reaction
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeReaction(Reaction $reaction)
    {
        return $this->reactions->removeElement($reaction);
    }
}
