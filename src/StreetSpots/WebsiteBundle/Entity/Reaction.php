<?php

namespace StreetSpots\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reaction
 *
 * @ORM\Table(name="reaction")
 * @ORM\Entity(repositoryClass="StreetSpots\WebsiteBundle\Repository\ReactionRepository")
 */
class Reaction implements EntityInterface
{
    use EntityTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var ReactionType
     *
     * @ORM\ManyToOne(targetEntity="ReactionType")
     * @ORM\JoinColumn(name="reaction_type_id", referencedColumnName="id")
     */
    private $reactionType;

    /**
     * @var string
     *
     * @ORM\Column(name="yelp_id", type="string", length=255, nullable=true)
     */
    private $yelpId;

    /**
     * @var string
     *
     * @ORM\Column(name="yelp_user_id", type="string", length=255, nullable=true)
     */
    private $yelpUserId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user.
     *
     * @param User|null $user
     *
     * @return Reaction
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set reactionType.
     *
     * @param ReactionType|null $reactionType
     *
     * @return Reaction
     */
    public function setReactionType(ReactionType $reactionType = null)
    {
        $this->reactionType = $reactionType;

        return $this;
    }

    /**
     * Get reactionType.
     *
     * @return ReactionType|null
     */
    public function getReactionType()
    {
        return $this->reactionType;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->getReactionType()->getName();
    }
}
