<?php

namespace StreetSpots\WebsiteBundle\Entity\Behavior;

use DoctrineExtensions\Taggable\Taggable as BaseTaggable;

interface Taggable extends BaseTaggable
{
}
