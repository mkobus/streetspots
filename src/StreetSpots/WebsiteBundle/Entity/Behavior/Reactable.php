<?php

namespace StreetSpots\WebsiteBundle\Entity\Behavior;

use Doctrine\Common\Collections\ArrayCollection;
use StreetSpots\WebsiteBundle\Entity\Reaction;

interface Reactable
{
    /**
     * @return Reaction[]|ArrayCollection
     */
    public function getLikes();

    /**
     * @return Reaction[]|ArrayCollection
     */
    public function getDislikes();

    /**
     * @return Reaction[]|ArrayCollection
     */
    public function getReactions();

    /**
     * @param Reaction[]|ArrayCollection $reactions
     *
     * @return static
     */
    public function setReactions(ArrayCollection $reactions);

    /**
     * @param Reaction $reaction
     *
     * @return static
     */
    public function addReaction(Reaction $reaction);

    /**
     * @param Reaction $reaction
     *
     * @return static
     */
    public function removeReaction(Reaction $reaction);
}
