<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 17.10.2016
 * Time: 20:48
 */

namespace StreetSpots\WebsiteBundle\Entity\Behavior;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use StreetSpots\WebsiteBundle\Entity\Reaction;
use StreetSpots\WebsiteBundle\Repository\ReactionTypeRepository;

trait ReactableTrait
{
    /**
     * @var Reaction[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Reaction", cascade={"persist", "remove"})
     */
    protected $reactions;

    /**
     * @return Reaction[]|ArrayCollection
     */
    public function getLikes()
    {
        if (null === $this->reactions) {
            $this->reactions = new ArrayCollection();
        }

        return $this->reactions->filter(
            function ($entity) {
                return $entity->getReactionType()->getId() == ReactionTypeRepository::TYPE_LIKE;
            }
        );
    }

    /**
     * @return Reaction[]|ArrayCollection
     */
    public function getDislikes()
    {
        if (null === $this->reactions) {
            $this->reactions = new ArrayCollection();
        }

        return $this->reactions->filter(
            function ($entity) {
                return $entity->getReactionType()->getId() == ReactionTypeRepository::TYPE_DISLIKE;
            }
        );
    }

    /**
     * @return Reaction[]|ArrayCollection
     */
    public function getReactions()
    {
        if (null === $this->reactions) {
            $this->reactions = new ArrayCollection();
        }

        return $this->reactions;
    }

    /**
     * @param Reaction[]|ArrayCollection $reactions
     *
     * @return static
     */
    public function setReactions(ArrayCollection $reactions)
    {
        if (null === $this->reactions) {
            $this->reactions = new ArrayCollection();
        }

        $this->reactions = $reactions;

        return $this;
    }

    /**
     * @param Reaction $reaction
     *
     * @return static
     */
    public function addReaction(Reaction $reaction)
    {
        if (null === $this->reactions) {
            $this->reactions = new ArrayCollection();
        }

        $this->reactions->add($reaction);

        return $this;
    }

    /**
     * @param Reaction $reaction
     *
     * @return static
     */
    public function removeReaction(Reaction $reaction)
    {
        $this->reactions->removeElement($reaction);

        return $this;
    }
}
