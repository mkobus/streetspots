<?php

namespace StreetSpots\WebsiteBundle\Entity\Behavior;

use Doctrine\Common\Collections\ArrayCollection;
use StreetSpots\WebsiteBundle\Entity\Tag;

trait TaggableTrait
{
    /** @var Tag[] */
    private $tags;

    /**
     * @return string
     */
    function getTaggableType()
    {
        return static::class;
    }

    /**
     * @return int
     */
    function getTaggableId()
    {
        return $this->getId();
    }

    /**
     * @return Tag[]
     */
    function getTags()
    {
        $this->tags = $this->tags ?: new ArrayCollection();

        return $this->tags;
    }
}
