<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 20.03.2017
 * Time: 21:34
 */

namespace StreetSpots\WebsiteBundle\Controller\ParamConverter;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use StreetSpots\WebsiteBundle\Service\EntityService;
use Symfony\Component\HttpFoundation\Request;

class GenericEntityParamConverter implements ParamConverterInterface
{
    /** @var EntityService */
    protected $entityService;

    public function __construct(EntityService $entityService)
    {
        $this->entityService = $entityService;
    }

    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $entityTypeId = $request->attributes->get('entityTypeId');
        $entityId = $request->attributes->get('entityId');

        if (null === $entityTypeId || null === $entityId) {
            throw new \InvalidArgumentException('Route attributes are missing');
        }

        $entity = $this->entityService->getEntity($entityTypeId, $entityId);

        $request->attributes->set($configuration->getName(), $entity);
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        // TODO: Implement supports() method.

        return true;
    }

}
