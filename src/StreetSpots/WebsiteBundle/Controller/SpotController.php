<?php

namespace StreetSpots\WebsiteBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\SpotImage;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Form\Type\SpotType;
use StreetSpots\WebsiteBundle\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as FrameworkExtra;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class SpotController extends Controller
{
    /**
     * @FrameworkExtra\ParamConverter("spot", class="StreetSpotsWebsiteBundle:Spot", options={"mapping": {"slug": "slug"}})
     * @FrameworkExtra\Template
     *
     * @param Spot    $spot
     * @param Request $request
     *
     * @return array
     */
    public function viewAction(Spot $spot, Request $request)
    {
        $tagManager = $this->container->get('streetspots.tag.tag_manager');
        $tagManager->loadTagging($spot);

        return [
            'spot'         => $spot,
            'tags'         => $spot->getTags(),
            'neighborhood' => $this->container->get('streetspots.service.spot')
                ->findInRange($spot, 50, 6),
        ];
    }

    /**
     * @FrameworkExtra\Template
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $spotService = $this->container->get('streetspots.service.spot');
        $translator = $this->get('translator');

        $spot = $spotService->createEntity();
        $spot->setCreatedBy($this->getUser());

        $form = $this->createForm(SpotType::class, $spot);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager->persist($spot);
            $entityManager->flush();

            $this->addFlash(
                'success',
                $translator->trans('New Spot added successfully', [], 'streetspots.spot')
            );

            return $this->redirectToRoute('streetspots.spot', ['slug' => $spot->getSlug()]);
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
