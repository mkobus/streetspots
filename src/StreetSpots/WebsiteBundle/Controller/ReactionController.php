<?php

namespace StreetSpots\WebsiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as FrameworkExtra;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Repository\ReactionTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ReactionController extends Controller
{
    const DEFAULT_REDIRECT_ROUTE = 'streetspot.home';

    /**
     * @FrameworkExtra\ParamConverter("entity", converter="generic_entity_converter")
     * @FrameworkExtra\Template
     *
     * @param EntityInterface $entity
     * @param Request         $request
     *
     * @return RedirectResponse
     */
    public function reactAction(EntityInterface $entity, Request $request)
    {
        $type = $request->attributes->get('type');

        $likesService = $this->get('streetspots.service.reaction');
        $translator = $this->get('translator');

        switch ($type) {
            case ReactionTypeRepository::TYPE_LIKE:
                $likesService->like($this->getUser(), $entity);
                $this->addFlash('success', $translator->trans('Liked', [], 'user'));
                break;
            case ReactionTypeRepository::TYPE_DISLIKE:
                $likesService->dislike($this->getUser(), $entity);
                $this->addFlash('success', $translator->trans('Disliked', [], 'user'));
                break;
            default:
                return $this->redirectToRoute(static::DEFAULT_REDIRECT_ROUTE);
        }

        $referer = $request->headers->get('referer');

        if (null !== $referer) {
            return $this->redirect($referer);
        }

        return $this->redirectToRoute(static::DEFAULT_REDIRECT_ROUTE);
    }
}
