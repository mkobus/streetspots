<?php

namespace StreetSpots\WebsiteBundle\Controller;

use StreetSpots\WebsiteBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as FrameworkExtra;

class UserController extends Controller
{
    /**
     * @FrameworkExtra\ParamConverter("user", class="StreetSpotsWebsiteBundle:User", options={"mapping": {"username": "username"}})
     * @FrameworkExtra\Template
     *
     * @param User    $user
     * @param Request $request
     *
     * @return array
     */
    public function viewAction(User $user, Request $request)
    {
        return [
            'user'        => $user,
            'spots'       => $this->get('streetspots.service.spot')->findCreatedByUser($user),
            'rated_spots' => $this->get('streetspots.service.spot')->findRatedByUser($user),
        ];
    }

    /**
     * @FrameworkExtra\ParamConverter("user", class="StreetSpotsWebsiteBundle:User", options={"mapping": {"username": "username"}})
     *
     * @param User    $user
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function followAction(User $user, Request $request)
    {
        $userService = $this->get('streetspots.service.user');
        $translator = $this->get('translator');

        $userService->follow($this->getUser(), $user);

        $this->addFlash(
            'success',
            $translator->trans(
                'You are now following %username%',
                [
                    '%username%' => $user->getUsername(),
                ],
                'streetspots.user'
            )
        );

        $referer = $request->headers->get('referer');

        if (null !== $referer) {
            return $this->redirect($referer);
        }

        return $this->redirectToRoute('streetspots.home');
    }

    /**
     * @FrameworkExtra\ParamConverter("user", class="StreetSpotsWebsiteBundle:User", options={"mapping": {"username": "username"}})
     *
     * @param User    $user
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function unfollowAction(User $user, Request $request)
    {
        $userService = $this->get('streetspots.service.user');
        $translator = $this->get('translator');

        $userService->unfollow($this->getUser(), $user);

        $this->addFlash(
            'success',
            $translator->trans(
                'You are no longer following %username%',
                [
                    '%username%' => $user->getUsername(),
                ],
                'streetspots.user'
            )
        );

        $referer = $request->headers->get('referer');

        if (null !== $referer) {
            return $this->redirect($referer);
        }

        return $this->redirectToRoute('streetspots.home');
    }

    /**
     * @FrameworkExtra\Template
     *
     * @param int $page
     *
     * @return array
     */
    public function listAction($page)
    {
        $pagination = $this->get('knp_paginator')->paginate(
            $this->getDoctrine()->getRepository('StreetSpotsWebsiteBundle:User')->findAll(),
            $page,
            $this->container->getParameter('streetspots.view.items_per_page')
        );

        return ['users' => $pagination];
    }

    /**
     * @FrameworkExtra\ParamConverter("user", class="StreetSpotsWebsiteBundle:User", options={"mapping": {"username": "username"}})
     * @FrameworkExtra\Template
     *
     * @param User $user
     * @param int  $page
     *
     * @return array
     */
    public function followersAction(User $user, $page)
    {
        $pagination = $this->get('knp_paginator')->paginate(
            $user->getFollowers(),
            $page,
            $this->container->getParameter('streetspots.view.items_per_page')
        );

        return [
            'user'      => $user,
            'followers' => $pagination,
        ];
    }

    /**
     * @FrameworkExtra\ParamConverter("user", class="StreetSpotsWebsiteBundle:User", options={"mapping": {"username": "username"}})
     * @FrameworkExtra\Template
     *
     * @param User $user
     * @param int  $page
     *
     * @return array
     */
    public function followingAction(User $user, $page)
    {
        $pagination = $this->get('knp_paginator')->paginate(
            $user->getFollowing(),
            $page,
            $this->container->getParameter('streetspots.view.items_per_page')
        );

        return [
            'user'      => $user,
            'following' => $pagination,
        ];
    }

    /**
     * @FrameworkExtra\ParamConverter("user", class="StreetSpotsWebsiteBundle:User", options={"mapping": {"username": "username"}})
     * @FrameworkExtra\Template
     *
     * @param User    $user
     * @param         $page
     * @param Request $request
     *
     * @return array
     */
    public function spotsAction(User $user, $page, Request $request)
    {
        $spots = $this->get('knp_paginator')->paginate(
            $this->get('streetspots.service.spot')->findCreatedByUser($user),
            $page,
            $this->container->getParameter('streetspots.view.items_per_page')
        );

        return [
            'user'  => $user,
            'spots' => $spots,
        ];
    }

    /**
     * @FrameworkExtra\ParamConverter("user", class="StreetSpotsWebsiteBundle:User", options={"mapping": {"username": "username"}})
     * @FrameworkExtra\Template
     *
     * @param User    $user
     * @param         $page
     * @param Request $request
     *
     * @return array
     */
    public function ratedSpotsAction(User $user, $page, Request $request)
    {
        $spots = $this->get('knp_paginator')->paginate(
            $this->get('streetspots.service.spot')->findRatedByUser($user),
            $page,
            10
        );

        return [
            'user'  => $user,
            'spots' => $spots,
        ];
    }
}
