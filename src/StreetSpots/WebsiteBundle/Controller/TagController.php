<?php

namespace StreetSpots\WebsiteBundle\Controller;

use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as FrameworkExtra;

class TagController extends Controller
{
    /**
     * @FrameworkExtra\ParamConverter("tag", class="StreetSpotsWebsiteBundle:Tag", options={"mapping": {"slug": "slug"}})
     * @FrameworkExtra\Template
     *
     * @param Tag     $tag
     * @param Request $request
     *
     * @return array
     */
    public function viewAction(Tag $tag, $page, Request $request)
    {
        $spotService = $this->container->get('streetspots.service.spot');

        return [
            'tag'   => $tag,
            'spots' => $this->get('knp_paginator')->paginate(
                $spotService->findByTagPaginated($tag),
                $page,
                15
            ),
        ];
    }
}
