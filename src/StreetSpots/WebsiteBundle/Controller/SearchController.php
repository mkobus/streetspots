<?php

namespace StreetSpots\WebsiteBundle\Controller;

use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Form\Type\SearchType;
use StreetSpots\WebsiteBundle\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as FrameworkExtra;

class SearchController extends Controller
{
    const SEARCH_TYPE_TAG = 'tag';
    const SEARCH_TYPE_USER = 'user';

    /**
     * @FrameworkExtra\Template
     *
     * @param string  $type
     * @param string  $query
     * @param int     $page
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function searchAction($type, $query, $page, Request $request)
    {
        /** @var int $itemsPerPage */
        $itemsPerPage = $this->container->getParameter('streetspots.view.items_per_page');
        /** @var SearchService $searchService */
        $searchService = $this->get('streetspots.service.search');

        $users = [];
        $tags = [];
        $data = [
            'type'  => $type,
            'query' => $query,
        ];

        $searchForm = $this->createForm(SearchType::class, $data);
        $searchForm->handleRequest($request);

        if ($searchForm->isValid()) {
            $data = $searchForm->getData();

            return $this->redirectToRoute('streetspots.search', ['type' => $data['type'], 'query' => $data['query'], 'page' => 1]);
        }

        if (!empty($query)) {
            if (static::SEARCH_TYPE_USER === $type) {
                $users = $this->get('knp_paginator')->paginate(
                    $searchService->findUsersByUsernamePaginated($query, false),
                    $page,
                    $itemsPerPage
                );
            } else {
                $tags = $this->get('knp_paginator')->paginate(
                    $searchService->findTagsByNamePaginated($query, false),
                    $page,
                    $itemsPerPage
                );
            }
        }

        return [
            'form'  => $searchForm->createView(),
            'type'  => $type,
            'query' => $query,
            'page'  => $page,
            'users' => $users,
            'tags'  => $tags,
        ];
    }
}
