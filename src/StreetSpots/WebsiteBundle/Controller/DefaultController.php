<?php

namespace StreetSpots\WebsiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use StreetSpots\RecommendationBundle\Recommender\Algorithm\KNearestNeighbors;
use StreetSpots\RecommendationBundle\Recommender\Dataset\DoctrineDataset;
use StreetSpots\RecommendationBundle\Recommender\Similarity\PearsonCorrelationSimilarity;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction()
    {
        $user = $this->getUser();

        return ['user' => $user];
    }

    /**
     * @Template()
     *
     * @param int $page
     *
     * @return array
     */
    public function exploreAction($page)
    {
        /** @var User $user */
        $user = $this->getUser();

        $dataset = new DoctrineDataset($this->get('doctrine.orm.entity_manager'));
        $similarity = new PearsonCorrelationSimilarity();
        $recommender = new KNearestNeighbors($similarity, $dataset, 15);

        $recommendations = $recommender->recommend($user->getId(), 60);

        $spots = $this->get('streetspots.repository.spot')->findBy(['id' => array_keys($recommendations)]);
        $spotIds = array_map(
            function (Spot $spot) {
                return $spot->getId();
            },
            $spots
        );
        $spotsById = array_combine($spotIds, $spots);

        $topRecommendedSpots = [];
        /** @var Spot $spot */
        foreach ($recommendations as $spotId => $score) {
            $topRecommendedSpots[] = $spotsById[$spotId];
        }

        return [
            'spots' => $this->get('knp_paginator')->paginate(
                $topRecommendedSpots,
                $page,
                15
            ),
        ];
    }
}
