<?php

namespace StreetSpots\WebsiteBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        /** @var ItemInterface|ItemInterface[] $menu */
        $menu = $factory->createItem(
            'root',
            ['childrenAttributes' => ['class' => 'navbar-nav nav-fill w-100 align-items-start']]
        );

        $labelClass = 'tab-label';
        $itemClass = 'nav-item';
        $linkClass = 'nav-link';

        $menu
            ->addChild('Neighborhood', ['route' => 'streetspots.home'])
            ->setAttribute('class', $itemClass)
            ->setLabelAttribute('class', $labelClass)
            ->setLinkAttribute('class', $linkClass)
            ->setExtra('icon', 'home');
        $menu
            ->addChild('Search', ['route' => 'streetspots.search'])
            ->setAttribute('class', $itemClass)
            ->setLabelAttribute('class', $labelClass)
            ->setLinkAttribute('class', $linkClass)
            ->setExtra('icon', 'search');
        $menu
            ->addChild('Add', ['route' => 'streetspots.spot.create'])
            ->setAttribute('class', $itemClass)
            ->setLabelAttribute('class', $labelClass)
            ->setLinkAttribute('class', $linkClass)
            ->setExtra('icon', 'plus');
        $menu
            ->addChild('Explore', ['route' => 'streetspots.explore'])
            ->setAttribute('class', $itemClass)
            ->setLabelAttribute('class', $labelClass)
            ->setLinkAttribute('class', $linkClass)
            ->setExtra('icon', 'star');
        $menu
            ->addChild('Profile', ['route' => 'streetspots.user', 'routeParameters' => ['username' => $user->getUsername()]])
            ->setAttribute('class', $itemClass)
            ->setLabelAttribute('class', $labelClass)
            ->setLinkAttribute('class', $linkClass)
            ->setExtra('icon', 'user');

        $menu['Profile']
            ->setChildrenAttribute('class', 'navbar-nav ml-auto w-100 justify-content-end')
            ->addChild('Edit', ['route' => 'streetspots.user.edit', 'routeParameters' => ['username' => $user->getUsername()]])
            ->setAttribute('class', $itemClass)
            ->setLinkAttribute('class', 'nav-link')
            ->setExtra('icon', 'gear')
            ->setLabel(false);

        return $menu;
    }
}
