<?php

namespace StreetSpots\WebsiteBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author Maciej Kobus <maciej@mkobus.com>
 */
class LinesNumberValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LinesNumber) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\LinesNumber');
        }

        if (null === $value) {
            return;
        }

        $linesOfText = substr_count($value, PHP_EOL) + 1;

        if ($linesOfText > $constraint->lines) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%value%', $this->formatValue($value))
                ->setParameter('%lines%', $constraint->lines)
                ->setCode(LinesNumber::MAX_LINES_NUMBER_EXCEEDED)
                ->addViolation();

            return;
        }
    }
}