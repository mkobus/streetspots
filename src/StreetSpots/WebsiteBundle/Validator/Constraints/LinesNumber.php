<?php
/**
 * Created by PhpStorm.
 * User: makb
 * Date: 30.11.16
 * Time: 14:28
 */

namespace StreetSpots\WebsiteBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Maciej Kobus <maciej@mkobus.com>
 */
class LinesNumber extends Constraint
{
    const MAX_LINES_NUMBER_EXCEEDED = 'ba785a8d-82cb-4283-967c-3cf342181b40';

    public $message = 'Lines number exceeds %lines%';

    public $lines;

    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
        return 'lines';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return ['lines'];
    }
}