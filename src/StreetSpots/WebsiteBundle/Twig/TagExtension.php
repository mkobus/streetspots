<?php

namespace StreetSpots\WebsiteBundle\Twig;

use StreetSpots\WebsiteBundle\Entity\Behavior\Taggable;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Tag\Autolinker;
use StreetSpots\WebsiteBundle\Tag\StringExtractor;
use StreetSpots\WebsiteBundle\Tag\TagManager;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

class TagExtension extends Twig_Extension
{
    /** @var Autolinker */
    protected $autolinker;

    /**
     * @param Autolinker $autolinker
     */
    public function __construct(Autolinker $autolinker)
    {
        $this->autolinker = $autolinker;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction(
                'autolink_tags',
                [$this, 'autolinkTags'],
                [
                    'needs_environment' => true,
                    'is_safe'           => ['html'],
                ]
            ),
        ];
    }

    /**
     * @param Twig_Environment $twig
     * @param string           $text
     *
     * @return string
     */
    public function autolinkTags(Twig_Environment $twig, $text)
    {
        return $this->autolinker->autolinkTags($text);
    }
}
