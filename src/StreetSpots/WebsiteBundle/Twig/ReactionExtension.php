<?php

namespace StreetSpots\WebsiteBundle\Twig;

use StreetSpots\WebsiteBundle\Entity\Behavior\Reactable;
use StreetSpots\WebsiteBundle\Service\ReactionService;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

class ReactionExtension extends Twig_Extension
{
    /** @var ReactionService */
    protected $reactionService;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /**
     * @param ReactionService       $reactionService
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ReactionService $reactionService, TokenStorageInterface $tokenStorage)
    {
        $this->reactionService = $reactionService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction(
                'reactionsWidget',
                [$this, 'reactionsWidget'],
                [
                    'needs_environment' => true,
                    'is_safe'           => ['html'],
                ]
            ),
        ];
    }

    /**
     * @param Twig_Environment $twig
     * @param Reactable        $entity
     *
     * @return string
     */
    public function reactionsWidget(Twig_Environment $twig, Reactable $entity)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $userReaction = $this->reactionService->getUserReaction($user, $entity);

        return $twig->render(
            '@StreetSpotsWebsite/ReactionWidget/widget.html.twig',
            [
                'likes'        => $entity->getLikes()->count(),
                'dislikes'     => $entity->getDislikes()->count(),
                'entity'       => $entity,
                'userReaction' => $userReaction,
            ]
        );
    }
}
