<?php

namespace StreetSpots\WebsiteBundle\Twig;

use StreetSpots\WebsiteBundle\Entity\Behavior\Reactable;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Service\ReactionService;
use StreetSpots\WebsiteBundle\Service\UserService;
use StreetSpots\WebsiteBundle\Service\UsersRelationshipService;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

class UserExtension extends Twig_Extension
{
    /** @var UsersRelationshipService */
    protected $usersRelationshipService;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /**
     * @param UsersRelationshipService $usersRelationshipService
     * @param TokenStorageInterface    $tokenStorage
     */
    public function __construct(UsersRelationshipService $usersRelationshipService, TokenStorageInterface $tokenStorage)
    {
        $this->usersRelationshipService = $usersRelationshipService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction(
                'user_is_following',
                [$this, 'userIsFollowing'],
                [
                    'is_safe' => ['html'],
                ]
            ),
        ];
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public function userIsFollowing(User $user)
    {
        /** @var User $follower */
        $follower = $this->tokenStorage->getToken()->getUser();

        return $this->usersRelationshipService->isUserFollowing($follower, $user);
    }
}
