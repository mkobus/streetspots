<?php

namespace StreetSpots\WebsiteBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use DoctrineExtensions\Taggable\TagManager;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Tag\StringExtractor;

class SaveTagsOnSpotUpdateListener
{
    /** @var TagManager */
    protected $tagManager;

    /** @var StringExtractor */
    protected $stringExtractor;

    /**
     * @param TagManager      $tagManager
     * @param StringExtractor $stringExtractor
     */
    public function __construct(TagManager $tagManager, StringExtractor $stringExtractor)
    {
        $this->tagManager = $tagManager;
        $this->stringExtractor = $stringExtractor;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if (!$object instanceof Spot) {
            return;
        }

        $this->saveTagging($object);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if (!$object instanceof Spot) {
            return;
        }

        $this->saveTagging($object);

    }

    private function saveTagging(Spot $spot)
    {
        $extractedTags = $this->stringExtractor->extractHashtags($spot->getDescription());
        $tags = $this->tagManager->loadOrCreateTags($extractedTags);

        $this->tagManager->addTags($tags, $spot);
        $this->tagManager->saveTagging($spot);

    }
}
