<?php

namespace StreetSpots\RecommendationBundle\Command;

use StreetSpots\RecommendationBundle\Recommender\Algorithm\WeightedSlopeOne;
use StreetSpots\RecommendationBundle\Recommender\Dataset\DoctrineDataset;
use StreetSpots\RecommendationBundle\Recommender\Dataset\PreparedDataset;
use StreetSpots\WebsiteBundle\Entity\Spot;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RecommendSlopeOneCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('recommend:slope-one')
            ->setDescription('...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $dataset = new DoctrineDataset($this->getContainer()->get('doctrine.orm.entity_manager'));
        $dataset = new PreparedDataset();
        $recommender = new WeightedSlopeOne($dataset);


        $recommendations = $recommender->recommend('Jack Matthews', 100);

//        var_dump($recommendations);
        var_dump($recommender->predict('Jack Matthews', 'Just My Luck'));
        die();

        $rows = [];
        foreach ($recommendations as $itemId => $rating) {
            $rows[] = [$itemId, $rating];
        }

//        $spots = $this->getContainer()->get('streetspots.repository.spot')->findBy(['id' => array_keys($recommendations)]);
//        $spotIds = array_map(function(Spot $spot) {return $spot->getId();}, $spots);

//        $spotsById = array_combine($spotIds, $spots);

//        $rows = [];
//        /** @var Spot $spot */
//        foreach ($recommendations as $spotId => $score) {
//            $rows[] = [$spotsById[$spotId], $score];
//        }

        $table = new Table($output);
        $table
            ->setHeaders(['ItemName', 'Score'])
            ->setRows($rows);
        $table->render();

        $output->writeln('Memory peak: '.round(memory_get_peak_usage() / 1048576, 2).' MB');
    }
}

