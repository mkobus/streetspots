<?php

namespace StreetSpots\RecommendationBundle\Command;

use StreetSpots\RecommendationBundle\Recommender\Algorithm\KNearestNeighbors;
use StreetSpots\RecommendationBundle\Recommender\Algorithm\SlopeOne;
use StreetSpots\RecommendationBundle\Recommender\Algorithm\WeightedSlopeOne;
use StreetSpots\RecommendationBundle\Recommender\Dataset\ConstructedDataset;
use StreetSpots\RecommendationBundle\Recommender\Dataset\DoctrineDataset;
use StreetSpots\RecommendationBundle\Recommender\Evaluation\MAE;
use StreetSpots\RecommendationBundle\Recommender\Evaluation\RMSE;
use StreetSpots\RecommendationBundle\Recommender\Similarity\CosineSimilarity;
use StreetSpots\RecommendationBundle\Recommender\Similarity\EuclideanSimilarity;
use StreetSpots\RecommendationBundle\Recommender\Similarity\JaccardIndex;
use StreetSpots\RecommendationBundle\Recommender\Similarity\PearsonCorrelationSimilarity;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Stopwatch\StopwatchPeriod;

class RecommenderEvaluateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('recommender:evaluate')
            ->setDescription('Evaluate Recommendations')
            ->addOption('csv', null, InputOption::VALUE_OPTIONAL, 'Export to CSV', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $csv = $input->getOption('csv');
        $csv = null === $csv;
        $fileNamePrefix = date('dmy') . '_' . date('His');

        $evaluationResults = [];
        $stopwatch = new Stopwatch(true);
        $rmse = new RMSE();
        $mae = new MAE();

        $cache = new FilesystemCache();
        if ($cache->has('rs.sample_set') && $cache->has('rs.training_set') && $cache->has('rs.items')) {
            $sampleSet = $cache->get('rs.sample_set');
            $trainingSet = $cache->get('rs.training_set');
            $items = $cache->get('rs.items');
        } else {
            $dataset = new DoctrineDataset($this->getContainer()->get('doctrine.orm.entity_manager'));

            $trainingSet = $dataset->getUserRatings();
            $items = $dataset->getItems();
            $sampleSet = [];
            foreach ($trainingSet as $userId => $itemRatings) {
                foreach ($itemRatings as $itemId => $rating) {
                    if (random_int(0, 10) === 3) {
                        if (!isset($sampleSet[$userId])) {
                            $sampleSet[$userId] = [];
                        }

                        $sampleSet[$userId][$itemId] = $rating;
                        $trainingSet[$userId][$itemId] = 0;
                        unset($trainingSet[$userId][$itemId]);

                        if (empty($trainingSet[$userId])) {
                            unset ($trainingSet[$userId]);
                        }
                    }
                }
            }

            $cache->set('rs.sample_set', $sampleSet);
            $cache->set('rs.training_set', $trainingSet);
            $cache->set('rs.items', $items);
        }


        // stats on set sizes
        $table = new Table($output);
        $table->setHeaders(['Training set', 'Sample set']);
        $table->addRow(
            [
                array_sum(array_map('\count', $trainingSet)),
                array_sum(array_map('\count', $sampleSet)),
            ]
        );
        $table->render();

        $sampleSetCount = array_sum(array_map('\count', $sampleSet));
        $dataset = new ConstructedDataset($trainingSet, $items);

        $matrix = $this->composeMatrix($trainingSet, $sampleSet);

        $recommenders = [
            // Cosine
            'cosine_3' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 3),
            'cosine_10' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 10),
            'cosine_20' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 20),
            'cosine_100' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 100),
            'cosine_200' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 200),
            'cosine_250' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 250),
            'cosine_500' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 500),
            // Pearson
            'pearson_3' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 3),
            'pearson_10' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 10),
            'pearson_20' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 20),
            'pearson_100' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 100),
            'pearson_200' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 200),
            'pearson_250' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 250),
            'pearson_500' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 500),
            // Euclidean
            'euclidean_3' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 3),
            'euclidean_10' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 10),
            'euclidean_20' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 20),
            'euclidean_100' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 100),
            'euclidean_200' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 200),
            'euclidean_250' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 250),
            'euclidean_500' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 500),
            // Jaccard
            'jaccard_3' => new KNearestNeighbors(new JaccardIndex(), $dataset, 3),
            'jaccard_10' => new KNearestNeighbors(new JaccardIndex(), $dataset, 10),
            'jaccard_20' => new KNearestNeighbors(new JaccardIndex(), $dataset, 20),
            'jaccard_100' => new KNearestNeighbors(new JaccardIndex(), $dataset, 100),
            'jaccard_200' => new KNearestNeighbors(new JaccardIndex(), $dataset, 200),
            'jaccard_250' => new KNearestNeighbors(new JaccardIndex(), $dataset, 250),
            'jaccard_500' => new KNearestNeighbors(new JaccardIndex(), $dataset, 500),
            // Slope One
            'slope_one' => new SlopeOne($dataset),
            'weighted_slope_one' => new WeightedSlopeOne($dataset),
        ];

        foreach ($recommenders as $key => $recommender) {
            gc_collect_cycles();
            $stopwatchName = 'prediction_' . $key;
            $startMem = memory_get_usage();

            $output->writeln('Started predicting ' . $key  . ' set');


            $predictionMatrix = $matrix;
            $ratings = [];
            $estimations = [];
            $estimationStopwatch = $stopwatch->start($stopwatchName, 'evaluation');
            $progressBar = new ProgressBar($output, $sampleSetCount);
            $progressBar->start();
            foreach ($sampleSet as $userId => $itemRating) {
                foreach ($itemRating as $itemId => $rating) {
                    $prediction = $recommender->predict($userId, $itemId);
                    $ratings[] = $rating;
                    $estimations[] = (int) round($prediction);

                    $predictionMatrix[$userId][$itemId] = $rating . ' / ' . ($prediction ?? '~');

                    $stopwatch->lap($stopwatchName);
                    $progressBar->advance();
                }
            }
            $stopwatch->stop($stopwatchName);
            $finalMem = $estimationStopwatch->getMemory();
            $progressBar->finish();

            if ($csv) {
                $itemKeys = [];
                foreach ($predictionMatrix as $userId => $itemRating) {
                    array_unshift($predictionMatrix[$userId], $userId);
                    $itemKeys = array_keys($itemRating);
                }
                array_unshift($itemKeys, '');
                array_unshift($predictionMatrix, $itemKeys);
                $fileName = sprintf('var/exports/%s/matrix_%s.csv', $fileNamePrefix, $key);
                $fs = new Filesystem();
                $fs->dumpFile(
                    $fileName,
                    $this->getContainer()->get('serializer')->encode($predictionMatrix, 'csv')
                );
                $output->writeln(['', 'Saved matrix CSV file: ' . $fileName]);
            }

            $estimationPeriods = $estimationStopwatch->getPeriods();
            $sum = array_sum(array_map(function (StopwatchPeriod $val) {
                return $val->getDuration();
            }, $estimationPeriods));
            $average = $sum / \count($estimationPeriods);

            $evaluationResults[] = [
                'Set ' . $key,
                $rmse->evaluate($ratings, $estimations),
                $mae->evaluate($ratings, $estimations),
                $average,
                $estimationStopwatch->getDuration(),
                $this->formatMemoryUsage($finalMem, $startMem),
                $this->getPredictionRatio($estimations),
                $this->getPerfectPredictionsCount($estimations, $ratings),
                $this->getMatrixTense($matrix) . '%',
            ];

            $output->writeln(['', 'Finished in ' . $estimationStopwatch->getDuration() . ' ms']);
        }

        $resultsTable = new Table($output);
        $resultsTable
            ->setHeaders([
                'Technique',
                'RMSE',
                'MAE',
                'Average prediction time (ms)',
                'Total time (ms)',
                'Memory (MB)',
                'Prediction ratio (%)',
                '# of perfect predictions',
                'Tense',
            ]);

        $resultsTable->addRows($evaluationResults);
        $resultsTable->render();

        if ($csv) {
            $fileName = sprintf('var/exports/%s/evaluations.csv', $fileNamePrefix);
            $fs = new Filesystem();
            $fs->dumpFile(
                $fileName,
                $this->getContainer()->get('serializer')->encode($evaluationResults, 'csv')
            );
            $output->writeln(['', 'Saved CSV file: ' . $fileName]);
        }
        gc_collect_cycles();
    }

    /**
     * @param $endMemory
     * @param $startMemory
     *
     * @return float
     */
    protected function formatMemoryUsage($endMemory, $startMemory): float
    {
        return round(((float)$endMemory - (float)$startMemory) / (float)(1024 * 1024), 3);
    }

    private function getPredictionRatio(array $estimations): float
    {
        $predicted = \count(array_filter($estimations, function ($value) { return $value > 0; }));

        return round(($predicted / \count($estimations)) * 100, 2);
    }

    private function getPerfectPredictionsCount(array $estimations, array $ratings): int
    {
        $numOfPerfectPreds = 0;

        foreach ($estimations as $key => $estimation) {
            $numOfPerfectPreds = $estimation == $ratings[$key] ? $numOfPerfectPreds + 1 : $numOfPerfectPreds;
        }

        return $numOfPerfectPreds;
    }

    private function composeMatrix($trainingSet, $sampleSet)
    {
        $users = [];
        $items = [];

        foreach ($trainingSet as $userId => $itemRating) {
            $users[] = $userId;
            foreach ($itemRating as $itemId => $rating) {
                $items[] = $itemId;
            }
        }

        foreach ($sampleSet as $userId => $itemRating) {
            $users[] = $userId;
            foreach ($itemRating as $itemId => $rating) {
                $items[] = $itemId;
            }
        }

        $users = array_unique($users);
        $items = array_unique($items);

        $matrix = [];
        foreach ($users as $userId) {
            $matrix[$userId] = array_combine($items, array_fill(0, \count($items), null));
        }

        foreach ($matrix as $userId => $itemRating) {
            foreach ($itemRating as $itemId => $rating) {
                $matrix[$userId][$itemId] = $trainingSet[$userId][$itemId] ?? null;
            }
        }

        return $matrix;
    }

    private function getMatrixTense($trainingSet): float
    {
        $hasValue = 0;
        $users = array_keys($trainingSet);
        $items = [];
        $count = 0;
        foreach ($trainingSet as $itemRating) {
            foreach ($itemRating as $itemId => $rating) {
                $hasValue = !empty($rating) ? $hasValue + 1 : $hasValue;
                $items[] = $itemId;
                $count++;
            }
        }

        $items = array_unique($items);
        $dataCount = \count($users) * \count($items);

        return (float) round(((float) $hasValue / (float) $dataCount) * 100);
    }
}
