<?php

namespace StreetSpots\RecommendationBundle\Command;

use StreetSpots\RecommendationBundle\Recommender\Algorithm\KNearestNeighbors;
use StreetSpots\RecommendationBundle\Recommender\Algorithm\SlopeOne;
use StreetSpots\RecommendationBundle\Recommender\Algorithm\WeightedSlopeOne;
use StreetSpots\RecommendationBundle\Recommender\Dataset\ConstructedDataset;
use StreetSpots\RecommendationBundle\Recommender\Dataset\DoctrineDataset;
use StreetSpots\RecommendationBundle\Recommender\Evaluation\MAE;
use StreetSpots\RecommendationBundle\Recommender\Evaluation\RMSE;
use StreetSpots\RecommendationBundle\Recommender\Similarity\CosineSimilarity;
use StreetSpots\RecommendationBundle\Recommender\Similarity\EuclideanSimilarity;
use StreetSpots\RecommendationBundle\Recommender\Similarity\JaccardIndex;
use StreetSpots\RecommendationBundle\Recommender\Similarity\PearsonCorrelationSimilarity;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Stopwatch\StopwatchPeriod;

class RecommenderMeasureTimeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('recommender:measure-time')
            ->setDescription('Measure Recommendations Time')
            ->addOption('csv', null, InputOption::VALUE_OPTIONAL, 'Export to CSV', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $csv = $input->getOption('csv');
        $csv = null === $csv;
        $fileNamePrefix = date('dmy') . '_' . date('His');

        $evaluationResults = [];
        $stopwatch = new Stopwatch(true);

        $cache = new FilesystemCache();
        if ($cache->has('rs.sample_set') && $cache->has('rs.training_set') && $cache->has('rs.items')) {
            $sampleSet = $cache->get('rs.sample_set');
            $trainingSet = $cache->get('rs.training_set');
            $items = $cache->get('rs.items');
        } else {
            $dataset = new DoctrineDataset($this->getContainer()->get('doctrine.orm.entity_manager'));

            $trainingSet = $dataset->getUserRatings();
            $items = $dataset->getItems();
            $sampleSet = [];
            foreach ($trainingSet as $userId => $itemRatings) {
                foreach ($itemRatings as $itemId => $rating) {
                    if (random_int(0, 10) === 3) {
                        if (!isset($sampleSet[$userId])) {
                            $sampleSet[$userId] = [];
                        }

                        $sampleSet[$userId][$itemId] = $rating;
                        $trainingSet[$userId][$itemId] = 0;
                        unset($trainingSet[$userId][$itemId]);

                        if (empty($trainingSet[$userId])) {
                            unset ($trainingSet[$userId]);
                        }
                    }
                }
            }

            $cache->set('rs.sample_set', $sampleSet);
            $cache->set('rs.training_set', $trainingSet);
            $cache->set('rs.items', $items);
        }

        // stats on set sizes
        $table = new Table($output);
        $table->setHeaders(['Training set', 'Sample set']);
        $table->addRow(
            [
                array_sum(array_map('\count', $trainingSet)),
                array_sum(array_map('\count', $sampleSet)),
            ]
        );
        $table->render();

        $sampleSetCount = array_sum(array_map('\count', $sampleSet));
        $dataset = new ConstructedDataset($trainingSet, $items);

        $recommenders = [
            // Cosine
            'cosine_3' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 3),
            'cosine_10' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 10),
            'cosine_20' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 20),
            'cosine_100' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 100),
            'cosine_200' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 200),
            'cosine_250' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 250),
            'cosine_500' => new KNearestNeighbors(new CosineSimilarity(), $dataset, 500),
            // Pearson
            'pearson_3' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 3),
            'pearson_10' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 10),
            'pearson_20' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 20),
            'pearson_100' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 100),
            'pearson_200' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 200),
            'pearson_250' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 250),
            'pearson_500' => new KNearestNeighbors(new PearsonCorrelationSimilarity(), $dataset, 500),
            // Euclidean
            'euclidean_3' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 3),
            'euclidean_10' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 10),
            'euclidean_20' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 20),
            'euclidean_100' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 100),
            'euclidean_200' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 200),
            'euclidean_250' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 250),
            'euclidean_500' => new KNearestNeighbors(new EuclideanSimilarity(), $dataset, 500),
            // Jaccard
            'jaccard_3' => new KNearestNeighbors(new JaccardIndex(), $dataset, 3),
            'jaccard_10' => new KNearestNeighbors(new JaccardIndex(), $dataset, 10),
            'jaccard_20' => new KNearestNeighbors(new JaccardIndex(), $dataset, 20),
            'jaccard_100' => new KNearestNeighbors(new JaccardIndex(), $dataset, 100),
            'jaccard_200' => new KNearestNeighbors(new JaccardIndex(), $dataset, 200),
            'jaccard_250' => new KNearestNeighbors(new JaccardIndex(), $dataset, 250),
            'jaccard_500' => new KNearestNeighbors(new JaccardIndex(), $dataset, 500),
            // Slope One
            'slope_one' => new SlopeOne($dataset),
            'weighted_slope_one' => new WeightedSlopeOne($dataset),
        ];

        foreach ($recommenders as $key => $recommender) {
            gc_collect_cycles();
            $stopwatchName = 'prediction_' . $key;

            $output->writeln('Started predicting ' . $key  . ' set');

            $ratings = [];
            $estimations = [];
            $estimationStopwatch = $stopwatch->start($stopwatchName, 'evaluation');
            $numOfPasses = 10;
            $progressBar = new ProgressBar($output, $numOfPasses);
            $progressBar->start();
            $lapTimes = [];

            for ($i = 0; $i < $numOfPasses; $i++) {
                foreach ($sampleSet as $userId => $itemRating) {
                    foreach ($itemRating as $itemId => $rating) {
                        $prediction = $recommender->predict($userId, $itemId);
                        $ratings[] = $rating;
                        $estimations[] = $prediction;
                    }
                }
                $lap = $stopwatch->lap($stopwatchName);
                $laps = $lap->getPeriods();
                $lapTimes[] = array_pop($laps)->getDuration();
                $progressBar->advance();
            }

            $stopwatch->stop($stopwatchName);
            $progressBar->finish();

            $evaluationResults[] = [
                'Set ' . $key,
//                implode(', ', $lapTimes),
                array_sum($lapTimes) / count($lapTimes),
                array_sum($lapTimes) / (\count($lapTimes) * $sampleSetCount),
                min($lapTimes),
                min($lapTimes) / ($sampleSetCount),
            ];

            $output->writeln(['', 'Finished in ' . $estimationStopwatch->getDuration() . ' ms']);
        }

        $resultsTable = new Table($output);
        $resultsTable
            ->setHeaders([
                'Technique',
//                'First lap time',
                'Average total set prediction time (ms)',
                'Average single prediction time (ms)',
                'Minimal total prediction time (ms)',
                'Minimal single prediction time (ms)',
            ]);

        $resultsTable->addRows($evaluationResults);
        $resultsTable->render();

        if ($csv) {
            $fileName = sprintf('var/exports/%s/times.csv', $fileNamePrefix);
            $fs = new Filesystem();
            $fs->dumpFile(
                $fileName,
                $this->getContainer()->get('serializer')->encode($evaluationResults, 'csv')
            );
            $output->writeln(['', 'Saved CSV file: ' . $fileName]);
        }
        gc_collect_cycles();
    }

    /**
     * @param $endMemory
     * @param $startMemory
     *
     * @return float
     */
    protected function formatMemoryUsage($endMemory, $startMemory): float
    {
        return round(((float)$endMemory - (float)$startMemory) / (float)(1024 * 1024), 3);
    }
}
