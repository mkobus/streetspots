<?php

namespace StreetSpots\RecommendationBundle\Recommender\Similarity;


interface SimilarityInterface
{
    public function similarity(array $sample1, array $sample2): float;
}
