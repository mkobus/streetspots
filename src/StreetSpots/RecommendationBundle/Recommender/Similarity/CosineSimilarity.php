<?php

namespace StreetSpots\RecommendationBundle\Recommender\Similarity;


class CosineSimilarity implements SimilarityInterface
{
    public function similarity(array $sample1, array $sample2): float
    {
        $mutuallyRated = array_intersect(array_keys($sample1), array_keys($sample2));

        if (!count($mutuallyRated)) {
            return 0;
        }
        $dotProduct = 0.0;
        $sumA = 0.0;
        $sumB = 0.0;

        foreach ($mutuallyRated as $item) {
            $dotProduct += $sample1[$item] * $sample2[$item];
            $sumA += $sample1[$item] ** 2;
            $sumB += $sample2[$item] ** 2;
        }

        $norm = sqrt($sumA) * sqrt($sumB);

        if ($norm == 0) {
            return 0;
        }

        return $dotProduct / $norm;
    }
}

