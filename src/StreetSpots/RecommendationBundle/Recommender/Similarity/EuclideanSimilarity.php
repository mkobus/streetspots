<?php

namespace StreetSpots\RecommendationBundle\Recommender\Similarity;

/**
 * Calculates Euclidean distance based similarity.
 */
class EuclideanSimilarity implements SimilarityInterface
{
    public function similarity(array $sample1, array $sample2): float
    {
        $mutuallyRated = array_intersect(array_keys($sample1), array_keys($sample2));

        if (!count($mutuallyRated)) {
            return 0;
        }

        $sumOfSquares = 0;
        foreach ($mutuallyRated as $key) {
            $sumOfSquares += pow($sample1[$key] - $sample2[$key], 2);
        }

        return 1 / (sqrt($sumOfSquares) + 1);
    }
}
