<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 15.10.2017
 * Time: 19:33
 */

namespace StreetSpots\RecommendationBundle\Recommender\Similarity;


class JaccardIndex implements SimilarityInterface
{
    public function similarity(array $sample1, array $sample2): float
    {
        $vector1 = array_keys($sample1);
        $vector2 = array_keys($sample2);
        $commonElements = array_intersect($vector1, $vector2);
        $allElements = array_merge($vector1, $vector2);

        return count($commonElements) / count($allElements);
    }
}
