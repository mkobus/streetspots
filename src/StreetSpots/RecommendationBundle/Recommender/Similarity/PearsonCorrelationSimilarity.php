<?php

namespace StreetSpots\RecommendationBundle\Recommender\Similarity;


class PearsonCorrelationSimilarity implements SimilarityInterface
{
    public function similarity(array $sample1, array $sample2): float
    {
        $x = array_keys($sample1);
        $y = array_keys($sample2);

        $commonElements = array_intersect($x, $y);

        if (count($commonElements) <= 0) {
            return 0;
        }

        $meanX = array_sum($sample1) / count($sample1);
        $meanY = array_sum($sample2) / count($sample2);
        $axb = 0;
        $a2 = 0;
        $b2 = 0;

        foreach ($commonElements as $key) {
            $a = $sample1[$key] - $meanX;
            $b = $sample2[$key] - $meanY;
            $axb += ($a * $b);
            $a2 += pow($a, 2);
            $b2 += pow($b, 2);
        }

        $norm = sqrt((float)($a2 * $b2));
        if ($norm == 0) {
            return 0;
        }

        return $axb / $norm;
    }
}
