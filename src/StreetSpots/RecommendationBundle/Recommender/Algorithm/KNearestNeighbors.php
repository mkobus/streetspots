<?php

namespace StreetSpots\RecommendationBundle\Recommender\Algorithm;


use StreetSpots\RecommendationBundle\Recommender\Dataset\Dataset;
use StreetSpots\RecommendationBundle\Recommender\Recommender;
use StreetSpots\RecommendationBundle\Recommender\Similarity\SimilarityInterface;

/**
 * User based KNN.
 */
class KNearestNeighbors implements Recommender
{
    /** @var SimilarityInterface */
    protected $similarity;

    /** @var Dataset */
    protected $dataset;

    /** @var int */
    protected $k;

    /**
     * @param SimilarityInterface $similarity
     * @param Dataset             $dataset
     * @param int                 $k
     */
    public function __construct(SimilarityInterface $similarity, Dataset $dataset, int $k)
    {
        $this->dataset = $dataset;
        $this->similarity = $similarity;
        $this->k = $k;
    }

    /**
     * @param          $userId
     * @param int|null $amount
     *
     * @return array
     */
    public function recommend(int $userId, ?int $amount = null): array
    {
        $neighbors = $this->getNearestNeighbors($this->k, $this->dataset->getUserRatings(), $userId);
        $userRatings = $this->dataset->getRatingsForUser($userId);

        $totalScores = [];
        $sumSimilarity = [];
        $rankings = [];

        foreach ($neighbors as $neighnor => $similarity) {
            $neighborRatings = $this->dataset->getRatingsForUser($neighnor);

            foreach ($neighborRatings as $itemId => $rating) {
                if (isset($userRatings[$itemId]) && $userRatings[$itemId]) {
                    continue; /* ignore items user've already rated */
                }

                if (!isset($totalScores[$itemId])) {
                    $totalScores[$itemId] = 0;
                }

                if (!isset($sumSimilarity[$itemId])) {
                    $sumSimilarity[$itemId] = 0;
                }

                $totalScores[$itemId] += $rating * $similarity;
                $sumSimilarity[$itemId] += $similarity;
            }
        }

        foreach ($totalScores as $itemId => $score) {
            $rankings[$itemId] = $score / $sumSimilarity[$itemId];
        }

        arsort($rankings);

        return null === $amount
            ? $rankings
            : array_slice($rankings, 0, $amount, true);
    }

    public function predict(int $userId, int $itemId): float
    {
        $averageUserRating = $this->dataset->getAverageUserRating($userId);
        $usersWhoRatedItem = $this->dataset->getRatingsForItem($itemId);
        $trainingSet = array_intersect_key($this->dataset->getUserRatings(), $usersWhoRatedItem);
        $userRatings = $this->dataset->getRatingsForUser($userId);
        $neighbors = $this->getNearestNeighbors($this->k, $trainingSet, $userRatings);
        $similaritySum = 0.0;
        $absSum = 0.0;

        foreach ($neighbors as $neighborId => $similarity) {
            $averageNeighborRating = $this->dataset->getAverageUserRating($neighborId);
            $similaritySum += $similarity * ($this->dataset->getUserRatingForItem($neighborId, $itemId) - $averageNeighborRating);
            $absSum += abs($similarity);
        }

        if ($absSum == 0) {
            return 0;
        }

        $rating = $averageUserRating + ($similaritySum / $absSum);

        return $rating;
    }

    public function getNearestNeighbors(int $k, array $trainingSet, array $sampleSet): array
    {
        $scores = [];

        foreach ($trainingSet as $label => $ratings) {
            $similarity = $this->similarity->similarity($sampleSet, $ratings);
            $scores[$label] = $similarity;
        }

        arsort($scores, SORT_NUMERIC); // reverse sort

        // return top n (key = userId, value = similarity value)
        return array_slice($scores, 0, $k, true);
    }
}
