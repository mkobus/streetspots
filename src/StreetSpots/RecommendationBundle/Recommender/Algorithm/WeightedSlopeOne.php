<?php

namespace StreetSpots\RecommendationBundle\Recommender\Algorithm;

use StreetSpots\RecommendationBundle\Recommender\Dataset\Dataset;
use StreetSpots\RecommendationBundle\Recommender\Recommender;
use Symfony\Component\Cache\Simple\FilesystemCache;

class WeightedSlopeOne implements Recommender
{
    /** @var Dataset */
    protected $dataset;

    /** @var array */
    protected $dev;

    public function __construct(Dataset $dataset)
    {
        $this->dataset = $dataset;

        $this->buildDevMatrix();
    }

    public function recommend(int $userId, int $amount = null): array
    {
        $ratings = $this->dataset->getRatingsForUser($userId);

        $predictions = $this->doPredict($ratings);

        if (null === $amount) {
            return $predictions;
        }

        return array_slice($predictions, 0, $amount, true);
    }

    public function predict(int $userId, int $itemId): float
    {
        $userRatings = $this->dataset->getRatingsForUser($userId);

        $sumA = 0.0;
        $sumB = 0.0;

        foreach ($userRatings as $item => $rating) {
            if ($item === $itemId) {
                continue;
            }
            $item1 = $this->dataset->getRatingsForItem($item);
            $item2 = $this->dataset->getRatingsForItem($itemId);

            $freq = count(array_intersect_key($item1, $item2));

            $sumA += ($this->dev[$itemId][$item] + $rating) * $freq;
            $sumB += $freq;
        }

        if ($sumB == 0) {
            return 0;
        }

        return ($sumA / $sumB);
//        return (int) round($sumA / $sumB);
    }

    private function buildDevMatrix()
    {
        $itemRatings = $this->dataset->getItemRatings();
        $items = $this->dataset->getItems();

        $cache = new FilesystemCache();
        if ($cache->has('rs.slope_one.dev')) {
            $dev = $cache->get('rs.slope_one.dev');
        } else {
            $dev = [];
            foreach ($items as $item1) {
                foreach ($items as $item2) {
                    if ($item1 === $item2) {
                        continue;
                    }
                    $users1 = array_keys($itemRatings[$item1] ?? []);
                    $users2 = array_keys($itemRatings[$item2] ?? []);
                    $mutual = array_intersect($users1, $users2);

                    $dev[$item1][$item2] = 0.0;
                    foreach ($mutual as $user) {
                        $dev[$item1][$item2] += ($itemRatings[$item1][$user] - $itemRatings[$item2][$user]) / count($mutual);
                    }
                }
            }

            $cache->set('rs.slope_one.dev', $dev);
        }

        $this->dev = $dev;
    }
}
