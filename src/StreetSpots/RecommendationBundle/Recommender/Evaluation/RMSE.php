<?php

namespace StreetSpots\RecommendationBundle\Recommender\Evaluation;

class RMSE implements EvaluationInterface
{
    public function evaluate(array $ratings, array $estimations): float
    {
        $sum = 0;

        foreach ($ratings as $key => $rating) {
            $sum += pow(($estimations[$key] - $rating), 2);
        }

        return sqrt($sum / count($ratings));
    }
}
