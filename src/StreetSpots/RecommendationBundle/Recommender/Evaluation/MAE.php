<?php

namespace StreetSpots\RecommendationBundle\Recommender\Evaluation;

class MAE implements EvaluationInterface
{
    public function evaluate(array $ratings, array $estimations): float
    {
        $sum = 0;

        foreach ($ratings as $itemId => $rating) {
            $sum += abs($estimations[$itemId] - $rating);
        }

        return $sum / count($ratings);
    }
}
