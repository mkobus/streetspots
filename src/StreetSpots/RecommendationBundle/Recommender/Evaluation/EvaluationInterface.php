<?php

namespace StreetSpots\RecommendationBundle\Recommender\Evaluation;


interface EvaluationInterface
{
    public function evaluate(array $ratings, array $estimations): float;
}
