<?php

namespace StreetSpots\RecommendationBundle\Recommender\Normalization;


interface NormalizerInterface
{
    public function normalize(array $data): array;
}
