<?php

namespace StreetSpots\RecommendationBundle\Recommender\Normalization;

class FillZeroesNormalizer implements NormalizerInterface
{
    public function normalize(array $data): array
    {
        $keys = [];
        foreach ($data as $key => $value) {
            $keys = array_merge($keys, array_keys($value));
        }

        $uniqueKeys = array_unique($keys);
        $allValues = array_combine($uniqueKeys, array_fill(0, count($uniqueKeys), 0));

        foreach ($data as $key => $value) {
            $data[$key] = array_replace($allValues, $value);
        }

        return $data;
    }
}
