<?php

namespace StreetSpots\RecommendationBundle\Recommender;


interface Recommender
{
    public function recommend(int $userId, int $amount): array;

    public function predict(int $userId, int $itemId): float;
}
