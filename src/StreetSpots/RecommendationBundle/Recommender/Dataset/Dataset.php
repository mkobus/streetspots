<?php

namespace StreetSpots\RecommendationBundle\Recommender\Dataset;


abstract class Dataset
{
    /** @var array */
    protected $items;

    /** @var array */
    protected $userRatings;

    /** @var array */
    protected $itemRatings;

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param $userId
     * @param $itemId
     *
     * @return float|null
     */
    public function getUserRatingForItem($userId, $itemId): ?float
    {
        if (!isset($this->userRatings[$userId][$itemId])) {
            return null;
        }

        return $this->userRatings[$userId][$itemId];
    }

    /**
     * @param $userId
     *
     * @return array
     */
    public function getRatingsForUser($userId): array
    {
        return $this->userRatings[$userId];
    }

    /**
     * @param $itemId
     *
     * @return array
     */
    public function getRatingsForItem($itemId): array
    {
        if (!isset($this->itemRatings[$itemId])) {
            return [];
        }

        return $this->itemRatings[$itemId];
    }

    /**
     * @return array
     */
    public function getUserRatings(): array
    {
        return $this->userRatings;
    }

    /**
     * @return array
     */
    public function getItemRatings(): array
    {
        return $this->itemRatings;
    }

    /**
     * @param $userId
     *
     * @return float|int
     */
    public function getAverageUserRating($userId)
    {
        $ratings = $this->getRatingsForUser($userId);

        $sum = array_sum($ratings);

        if (!$sum) {
            return ($this->getRatingMin() + $this->getRatingMax()) / 2;
        }

        return $sum / count($ratings);
    }

    /**
     * @return int
     */
    public function getRatingMin()
    {
        return 1;
    }

    /**
     * @return int
     */
    public function getRatingMax()
    {
        return 5;
    }
}
