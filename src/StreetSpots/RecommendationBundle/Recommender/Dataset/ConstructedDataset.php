<?php

namespace StreetSpots\RecommendationBundle\Recommender\Dataset;

class ConstructedDataset extends Dataset
{
    public function __construct(array $userRatings, array $items)
    {
        $this->items = $items;
        $this->userRatings = $userRatings;

        foreach ($userRatings as $userId => $itemRating) {
            foreach ($itemRating as $itemId => $rating) {
                if (!isset($this->itemRatings[$itemId])) {
                    $this->itemRatings[$itemId] = [];
                }
                $this->itemRatings[$itemId][$userId] = $rating;
            }
        }
    }
}
