<?php

namespace StreetSpots\RecommendationBundle\Recommender\Dataset;

class PreparedDataset extends Dataset
{
    public function __construct()
    {
        $this->userRatings = $this->fetchUserRatings();
        $this->itemRatings = [];
    }

    /**
     * @return array
     */
    protected function fetchUserRatings(): array
    {
        return [
            'Lisa Rose' => [
                'Lady in the Water'  => 2.5,
                'Superman Returns'   => 3.5,
                'You, Me and Dupree' => 2.5,
            ],

            'Gene Seymour' => [
                'Lady in the Water'  => 3.0,
                'Snakes on a Plane'  => 3.5,
                'The Night Listener' => 3.0,
                'You, Me and Dupree' => 3.5,
            ],

            'Michael Phillips' => [
                'Lady in the Water'  => 2.5,
                'Snakes on a Plane'  => 3.0,
                'Superman Returns'   => 3.5,
                'The Night Listener' => 4.0,
            ],

            'Claudia Puig' => [
                'Snakes on a Plane'  => 3.5,
                'Just My Luck'       => 3.0,
                'Superman Returns'   => 4.0,
                'You, Me and Dupree' => 2.5,
            ],

            'Mick LaSalle' => [
                'Lady in the Water'  => 3.0,
                'Snakes on a Plane'  => 4.0,
                'Just My Luck'       => 2.0,
                'The Night Listener' => 3.0,
                'You, Me and Dupree' => 2.0,
            ],

            'Jack Matthews' => [
                'Lady in the Water'  => 3.0,
                'Snakes on a Plane'  => 4.0,
                'The Night Listener' => 3.0,
                'Superman Returns'   => 5.0,
            ],

            'Toby' => [
                'Superman Returns' => 2.0,
                'Gladiator'        => 5.0,
            ],
        ];
    }

    /**
     * @return array
     */
    protected function fetchItemRatings()
    {
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function parseArray(array $data, array $keys)
    {
        $result = [];

        foreach ($data as $row) {
            if (!isset($result[$row[$keys[0]]])) {
                $result[$row[$keys[0]]] = [];
            }

            if (null === $row[$keys[1]]) {
                continue;
            }

            $result[$row[$keys[0]]][$row[$keys[1]]] = $row[$keys[2]];
        }

        return $result;
    }
}
