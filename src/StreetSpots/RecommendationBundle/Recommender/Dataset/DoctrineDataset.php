<?php

namespace StreetSpots\RecommendationBundle\Recommender\Dataset;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

class DoctrineDataset extends Dataset
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->userRatings = $this->parseArray($this->fetchUserRatings(), ['user_id', 'spot_id', 'rating']);
        $this->itemRatings = $this->parseArray($this->fetchItemRatings(), ['spot_id', 'user_id', 'rating']);
        $this->items = array_column($this->fetchItems(), 'id');
    }

    /**
     * @return array
     */
    protected function fetchUserRatings(): array
    {
        $sql = 'SELECT u.id AS user_id, sr.spot_id, r.stars FROM users u
	LEFT JOIN ratings r ON r.user_id = u.id
	LEFT JOIN spot_rating sr ON sr.rating_id = r.id';

        $rsm = new Query\ResultSetMapping();
        $rsm->addScalarResult('user_id', 'user_id', 'string');
        $rsm->addScalarResult('spot_id', 'spot_id', 'string');
        $rsm->addScalarResult('stars', 'rating', 'string');

        $query = $this->entityManager->createNativeQuery($sql, $rsm);

        return $query->getArrayResult();
    }

    /**
     * @return array
     */
    protected function fetchItemRatings()
    {
        $sql = 'SELECT sr.spot_id, r.user_id, r.stars FROM ratings r
	LEFT JOIN spot_rating sr ON sr.rating_id = r.id';

        $rsm = new Query\ResultSetMapping();
        $rsm->addScalarResult('spot_id', 'spot_id', 'string');
        $rsm->addScalarResult('user_id', 'user_id', 'string');
        $rsm->addScalarResult('stars', 'rating', 'string');

        $query = $this->entityManager->createNativeQuery($sql, $rsm);

        return $query->getArrayResult();
    }

    /**
     * @return array
     */
    protected function fetchItems()
    {
        $sql = 'SELECT s.id FROM spot s';

        $rsm = new Query\ResultSetMapping();
        $rsm->addScalarResult('id', 'id', 'string');

        $query = $this->entityManager->createNativeQuery($sql, $rsm);

        return $query->getArrayResult();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function parseArray(array $data, array $keys)
    {
        $result = [];

        foreach ($data as $row) {
            if (!isset($result[$row[$keys[0]]])) {
                $result[$row[$keys[0]]] = [];
            }

            if (null === $row[$keys[1]]) {
                continue;
            }

            $result[$row[$keys[0]]][$row[$keys[1]]] = $row[$keys[2]];
        }

        return $result;
    }
}
