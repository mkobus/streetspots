<?php

namespace StreetSpots\DataBundle\Command;

use Faker\Generator;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Service\LocationService;
use StreetSpots\WebsiteBundle\Service\SpotService;
use StreetSpots\WebsiteBundle\Service\UserService;

class YelpImportBusinessesCommand extends AbstractYelpFileBased
{
    /** @var LocationService */
    protected $locationService;

    /** @var SpotService */
    protected $spotService;

    /** @var UserService */
    protected $userService;

    /** @var UserRepository */
    protected $userRepository;

    /** @var Generator */
    protected $faker;

    /** @var int */
    protected $userCount;

    public function getType(): string
    {
        return 'businesses';
    }

    public function process(int $index, array $data): ?EntityInterface
    {
        /** @var User $createdBy */
        $createdBy = $this->userRepository->findOneBy(['id' => rand(1, $this->userCount - 1)]);

        if (null === $createdBy) {
            return null;
        }

        $location = $this->locationService->createEntity();
        $location->setName(implode(', ', [$data['address'], $data['postal_code'].' '.$data['city'], $data['state']]));
        $location->setLatitude($data['latitude']);
        $location->setLongitude($data['longitude']);

        $tags = array_map(
            function ($category) {
                return '#'.preg_replace("/[^a-zA-Z0-9]+/", "", $category);
            },
            $data['categories']
        );

        $description = $this->faker->paragraph(2, true)
            .PHP_EOL.PHP_EOL
            .implode(' ', $tags);

        $spot = $this->spotService->createEntity();

        $spot->setName($data['name']);
        $spot->setDescription($description);
        $spot->setLocation($location);
        $spot->setCreatedBy($createdBy);
        $spot->setUpdatedBy($createdBy);
        $spot->setCreatedAt($this->faker->dateTime);
        $spot->setYelpId($data['business_id']);

        return $spot;
    }

    protected function setUp(): void
    {
        $this->userService = $this->getContainer()->get('streetspots.service.user');
        $this->userRepository = $this->getContainer()->get('streetspots.repository.user');
        $this->locationService = $this->getContainer()->get('streetspots.service.location');
        $this->spotService = $this->getContainer()->get('streetspots.service.spot');
        $this->faker = $this->getContainer()->get('nelmio_alice.faker.generator');
        $this->userCount = count($this->userRepository->findAll());
    }
}
