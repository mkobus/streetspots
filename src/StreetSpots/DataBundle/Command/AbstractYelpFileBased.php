<?php
/**
 * Created by PhpStorm.
 * User: maciejkobus
 * Date: 03.10.2017
 * Time: 22:08
 */

namespace StreetSpots\DataBundle\Command;


use Doctrine\ORM\EntityManager;
use Faker\Generator;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractYelpFileBased extends ContainerAwareCommand
{
    /** @var EntityManager */
    protected $entityManager;

    /** @var Generator */
    protected $faker;

    abstract public function getType(): string;

    abstract protected function process(int $index, array $data): ?EntityInterface;

    abstract protected function setUp(): void;

    protected function configure()
    {
        $this
            ->setName('yelp:import:'.$this->getType())
            ->setDescription(sprintf('Yelp dataset importer. Imports users from %s.json file.', $this->getType()))
            ->addArgument('file', InputArgument::REQUIRED, sprintf('%s.json file path', $this->getType()))
            ->addOption('limit-lines', null, InputOption::VALUE_OPTIONAL, 'limit lines', 0)
            ->addOption('ignore-lines', null, InputOption::VALUE_OPTIONAL, 'ignore first lines', 0);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp();

        $filePath = $input->getArgument('file');
        $ignoreLines = $input->getOption('ignore-lines');
        $limitLines = $input->getOption('limit-lines');

        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $totalLines = $this->getLines($filePath);
        $progress = new ProgressBar($output, $totalLines);

        $output->writeln(sprintf('<info>Will process %d entities.</info>', $totalLines));
        if ($ignoreLines > 0) {
            $output->writeln(sprintf('<info>First %d entities will be ignored.</info>', $ignoreLines));
        }

        $progress->start();

        $lineNumber = 0;
        $processedEntities = $ignoreLines;
        $stream = fopen($filePath, 'r');
        $batchSize = 20;
        $inBatch = 0;
        try {
            while (($line = fgets($stream)) !== false) {
                $lineNumber++;
                $progress->advance();
                if ($ignoreLines > $lineNumber) {
                    continue;
                }

                if ($limitLines !== 0 && $lineNumber - $ignoreLines >= $limitLines) {
                    break;
                }

                $entity = $this->process($processedEntities + 2, json_decode($line, true));

                if (null !== $entity) {
                    $this->entityManager->persist($entity);
                }

                $processedEntities++;
                $inBatch++;

                if ($inBatch === $batchSize) {
                    $this->entityManager->flush();
                    $this->entityManager->clear();
                    $inBatch = 0;
                }
            }
            fclose($stream);
        } catch (\Exception $e) {
            fclose($stream);
            throw $e;
        }
        $this->entityManager->flush();

        $progress->finish();

        $output->writeln(
            [
                '',
                sprintf('<info>Proccessing done. %d entities proccessed.</info>', $processedEntities),
            ]
        );
    }

    private function getLines($file): int
    {
        $f = fopen($file, 'rb');
        $lines = 0;

        while (!feof($f)) {
            $lines += substr_count(fread($f, 8192), "\n");
        }

        fclose($f);

        return $lines;
    }
}
