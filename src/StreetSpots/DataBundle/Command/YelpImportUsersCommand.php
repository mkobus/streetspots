<?php

namespace StreetSpots\DataBundle\Command;

use DateTime;
use Faker\Generator;
use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Service\UserService;

class YelpImportUsersCommand extends AbstractYelpFileBased
{
    /** @var UserService */
    protected $userService;

    /** @var UserRepository */
    protected $userRepository;

    /** @var Generator */
    protected $faker;

    public function getType(): string
    {
        return 'users';
    }

    public function process(int $index, array $data): ?EntityInterface
    {
        $createdAt = DateTime::createFromFormat('Y-m-d', $data['yelping_since']);
        $user = $this->userService->createEntity();

        $user->setEmail($this->faker->unique()->email);
        $user->setUsername('user'.$index);
        $user->setPlainPassword('test');
        $user->setBio($this->faker->optional(0.7)->paragraph(2, true));
        $user->setWebsite($this->faker->optional(0.4)->url);
        $user->addRole('ROLE_USER');

        $user->setCreatedAt($createdAt);
        $user->setUpdatedAt($createdAt);

        $user->setYelpId($data['user_id']);
        $user->setYelpFriends($data['friends']);

        $this->entityManager->persist($user);

        return $user;
    }

    protected function setUp(): void
    {
        $this->userService = $this->getContainer()->get('streetspots.service.user');
        $this->userRepository = $this->getContainer()->get('streetspots.repository.user');
        $this->faker = $this->getContainer()->get('nelmio_alice.faker.generator');
    }
}
