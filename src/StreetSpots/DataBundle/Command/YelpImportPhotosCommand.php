<?php

namespace StreetSpots\DataBundle\Command;

use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Repository\SpotRepository;
use StreetSpots\WebsiteBundle\Service\SpotImageService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\File;

class YelpImportPhotosCommand extends AbstractYelpFileBased
{
    /** @var Filesystem */
    protected $filesystem;

    /** @var SpotRepository */
    protected $spotRepository;

    /** @var SpotImageService */
    protected $spotImageService;

    /** @var string */
    protected $photosDir;

    public function getType(): string
    {
        return 'photos';
    }

    protected function configure()
    {
        parent::configure();

        $this->addOption('photos-dir', null, InputOption::VALUE_REQUIRED, 'directory containing files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->photosDir = $input->getOption('photos-dir');

        parent::execute($input, $output);
    }

    public function process(int $index, array $data): ?EntityInterface
    {
        /** @var Spot $spot */
        $spot = $this->spotRepository->findOneBy(['yelpId' => $data['business_id']]);

        if (null === $spot) {
            return null;
        }

        $finder = new Finder();
        /** @var File|null $imageFile */
        $imageFile = $finder->files()->in($this->photosDir)->name($data['photo_id'].'.jpg')->getIterator()->current();

        if (null !== $imageFile) {
            $newFilePath = dirname($this->getContainer()->getParameter('kernel.root_dir')).'web/images/users/'.$imageFile->getFilename();
            $this->filesystem->copy($imageFile, $newFilePath);
            $imageFile = new File($newFilePath);
        }

        $spotImage = $this->spotImageService->createEntity();
        $spotImage->setSpot($spot);
        $spotImage->setImageFile($imageFile);

        return $spotImage;
    }

    protected function setUp(): void
    {
        $this->spotImageService = $this->getContainer()->get('streetspots.service.spot_image');
        $this->spotRepository = $this->getContainer()->get('streetspots.repository.spot');
        $this->filesystem = $this->getContainer()->get('filesystem');
    }
}
