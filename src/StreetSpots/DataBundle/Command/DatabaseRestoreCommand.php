<?php

namespace StreetSpots\DataBundle\Command;

use Doctrine\ORM\EntityManager;
use Faker\Generator;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class DatabaseRestoreCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('streetspots:database:restore')
            ->setDescription('Drops, creates and populates the database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->dropDatabase($output);
        $this->createDatabase($output);
        $this->createSchema($output);
        $this->loadDataFixtures($output);

    }

    private function dropDatabase(OutputInterface $output)
    {
        $command = $this->getApplication()->find('doctrine:database:drop');

        $arguments = [
            '--force' => true,
        ];

        $commandInput = new ArrayInput($arguments);

        return $command->run($commandInput, $output);
    }

    private function createDatabase(OutputInterface $output)
    {
        $command = $this->getApplication()->find('doctrine:database:create');


        $commandInput = new ArrayInput([]);

        return $command->run($commandInput, $output);
    }

    private function createSchema(OutputInterface $output)
    {
        $command = $this->getApplication()->find('doctrine:schema:create');


        $commandInput = new ArrayInput([]);

        return $command->run($commandInput, $output);
    }

    private function loadDataFixtures(OutputInterface $output)
    {
        $command = $this->getApplication()->find('doctrine:fixtures:load');

        $arguments = [
            '--no-interaction' => true,
        ];

        $commandInput = new ArrayInput($arguments);

        return $command->run($commandInput, $output);
    }
}
