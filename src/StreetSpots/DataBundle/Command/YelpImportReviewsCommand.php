<?php

namespace StreetSpots\DataBundle\Command;

use StreetSpots\WebsiteBundle\Entity\EntityInterface;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\User;
use StreetSpots\WebsiteBundle\Repository\SpotRepository;
use StreetSpots\WebsiteBundle\Repository\UserRepository;
use StreetSpots\WebsiteBundle\Service\ReactionService;

class YelpImportReviewsCommand extends AbstractYelpFileBased
{
    /** @var ReactionService */
    protected $reactionService;

    /** @var UserRepository */
    protected $userRepository;

    /** @var SpotRepository */
    protected $spotRepository;

    public function getType(): string
    {
        return 'reviews';
    }

    public function process(int $index, array $data): ?EntityInterface
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['yelpId' => $data['user_id']]);
        /** @var Spot $spot */
        $spot = $this->spotRepository->findOneBy(['yelpId' => $data['business_id']]);

        if ($data['stars'] > 3) {
            $this->reactionService->like($user, $spot);
        } elseif ($data['stars'] <= 3) {
            $this->reactionService->dislike($user, $spot);
        }

        return $spot;
    }

    protected function setUp(): void
    {
        $this->reactionService = $this->getContainer()->get('streetspots.service.reaction');
        $this->userRepository = $this->getContainer()->get('streetspots.repository.user');
        $this->spotRepository = $this->getContainer()->get('streetspots.repository.spot');
    }
}
