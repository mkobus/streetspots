<?php

namespace StreetSpots\DataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Fidry\AliceDataFixtures\Loader\PersisterLoader;
use Fidry\AliceDataFixtures\LoaderInterface;
use Nelmio\Alice\Loader\NativeLoader;
use StreetSpots\WebsiteBundle\Entity\Spot;
use StreetSpots\WebsiteBundle\Entity\Tag;
use StreetSpots\WebsiteBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class SpotsTagging extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    const MIN_NUM_OF_TAGS = 0;
    const MAX_NUM_OF_TAGS = 5;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        return;

        $tagManager = $this->container->get('streetspots.tag.tag_manager');
        $tagRepository = $manager->getRepository(Tag::class);

        $spots = $this->getSpotsFromManager($manager);
        $tags = $tagRepository->findAll();

        foreach ($spots as $spot) {
            $tagManager->addTags($this->getRandomElements($tags, mt_rand(static::MIN_NUM_OF_TAGS, static::MAX_NUM_OF_TAGS)), $spot);
            $tagManager->saveTagging($spot);
        }
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 3;
    }

    private function getRandomElements(array $array, $num)
    {
        if (0 === $num) {
            return [];
        }

        $keys = array_rand($array, $num);
        $elements = [];

        /* array_rand() returns string when $num === 1 */
        if (1 === $num) {
            $keys = [$keys];
        }

        foreach ($keys as $key) {
            $elements[] = $array[$key];
        }

        return $elements;
    }

    /**
     * @param ObjectManager $manager
     *
     * @return Spot[]
     */
    private function getSpotsFromManager(ObjectManager $manager): array
    {
        $userReferences = array_filter(
            $this->referenceRepository->getReferences(),
            function ($identitiy) {
                return 0 === strpos($identitiy, 'spot');
            },
            ARRAY_FILTER_USE_KEY
        );

        $spots = [];
        /** @var Spot $spotReference */
        foreach ($userReferences as $identity => $spotReference) {
            $spots[$identity] = $manager->find(Spot::class, $spotReference->getId());
        }

        return $spots;
    }
}
