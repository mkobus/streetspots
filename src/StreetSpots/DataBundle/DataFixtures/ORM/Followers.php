<?php

namespace StreetSpots\DataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Fidry\AliceDataFixtures\Loader\PersisterLoader;
use Fidry\AliceDataFixtures\LoaderInterface;
use Nelmio\Alice\Loader\NativeLoader;
use StreetSpots\WebsiteBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class Followers extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    const MIN_NUM_OF_RELATIONS = 0;
    const MAX_NUM_OF_RELATIONS = 20;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        return;

        $users = $this->getUsersFromManager($manager);

        if ($this->referenceRepository->hasReference('admin_user')) {
            $adminReference = $this->referenceRepository->getReference('admin_user');
            $users['admin_user'] = $manager->find(User::class, $adminReference->getId());
        }

        foreach ($users as $user) {
            $following = array_filter(
                $this->getRandomElements($users, mt_rand(static::MIN_NUM_OF_RELATIONS, static::MAX_NUM_OF_RELATIONS)),
                function ($value) use ($user) {
                    return $value->getId() !== $user->getId();
                }
            );
            $followers = array_filter(
                $this->getRandomElements($users, mt_rand(static::MIN_NUM_OF_RELATIONS, static::MAX_NUM_OF_RELATIONS)),
                function ($value) use ($user) {
                    return $value->getId() !== $user->getId();
                }
            );

            $user->setFollowing($following);
            $user->setFollowers($followers);

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 2;
    }

    private function getRandomElements(array $array, $num)
    {
        if (0 === $num) {
            return [];
        }

        $keys = array_rand($array, $num);
        $elements = [];

        /* array_rand() returns string when $num === 1 */
        if (1 === $num) {
            $keys = [$keys];
        }

        foreach ($keys as $key) {
            $elements[] = $array[$key];
        }

        return $elements;
    }

    /**
     * @param ObjectManager $manager
     *
     * @return User[]
     */
    private function getUsersFromManager(ObjectManager $manager): array
    {
        $userReferences = array_filter(
            $this->referenceRepository->getReferences(),
            function ($identitiy) {
                return 0 === strpos($identitiy, 'user');
            },
            ARRAY_FILTER_USE_KEY
        );

        $users = [];
        /** @var User $userReference */
        foreach ($userReferences as $identity => $userReference) {
            $users[$identity] = $manager->find(User::class, $userReference->getId());
        }

        return $users;
    }
}
