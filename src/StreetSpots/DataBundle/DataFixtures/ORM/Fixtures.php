<?php

namespace StreetSpots\DataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Fidry\AliceDataFixtures\Loader\PersisterLoader;
use Fidry\AliceDataFixtures\LoaderInterface;
use Nelmio\Alice\Loader\NativeLoader;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class Fixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->removeAssets();

        /** @var LoaderInterface $loader */
        $loader = $this->container->get('fidry_alice_data_fixtures.doctrine.persister_loader');
        $objects = $loader->load([__DIR__.'/../../Resources/fixtures/orm/fixtures.yml']);

        foreach ($objects as $name => $object) {
            $this->addReference($name, $object);
        }
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 1;
    }

    private function removeAssets()
    {
        $assetsDir = $this->container->getParameter('kernel.root_dir').'/../web/images/users';

        /** @var Filesystem $fileSystem */
        $fileSystem = $this->container->get('filesystem');

        if ($fileSystem->exists($assetsDir)) {
            $fileSystem->remove($assetsDir);
        }

        $fileSystem->mkdir($assetsDir);
    }
}
