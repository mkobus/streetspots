#!/usr/bin/env bash

ENV_FILE="/etc/profile.d/streetspots_app_env.sh"
ROOT_MYSQL_USER="root"
ROOT_MYSQL_PASS="secret"
SYMFONY__DATABASE__USER="streetspots"
SYMFONY__DATABASE__PASS="streetspots"
SYMFONY__DATABASE__NAME="streetspots"
DATA_WWW="/data/www"

# This is a hack to use env variables set by Homestead itself
touch ${ENV_FILE}
chmod a+x ${ENV_FILE}
ENV_SET_COMMANDS=`sed -n '/# Set Homestead Environment Variable/{n;p;}' /home/vagrant/.profile`
echo ${ENV_SET_COMMANDS} > ${ENV_FILE}
eval ${ENV_SET_COMMANDS}

echo "# Using env variables"
printenv | grep SYMFONY

echo "# Updating machine software"
locale-gen en_GB.UTF-8

echo "# Removing unused software"
sudo apt-get remove -y postgresql-9.4 php7.1-pgsql
sudo apt-get autoremove -y

echo "# Enabling and configuring XDebug"
cp /data/www/development/vm/configs/xdebug.ini /etc/php/7.1/mods-available/xdebug.ini

# Enabling Xdebug PHP module
sudo phpenmod -s fpm xdebug
sudo phpdismod -s cli xdebug

# Generating a applying proper permissions to xdebug log file
sudo touch /var/log/xdebug.log
sudo chown vagrant:vagrant /var/log/xdebug.log
sudo chmod 777 /var/log/xdebug.log

echo "# Configuring PHP"
sed -i 's/memory_limit = .*/memory_limit = 1024M/' /etc/php/7.1/cli/php.ini

echo "# Configuring MariaDB"
cp /data/www/development/vm/configs/mariadb.cnf /etc/mysql/conf.d

echo "# Creating database $SYMFONY__DATABASE__NAME and user $SYMFONY__DATABASE__USER"
mysql --user=${ROOT_MYSQL_USER} --password=${ROOT_MYSQL_PASS} -e "CREATE DATABASE IF NOT EXISTS \`$SYMFONY__DATABASE__NAME\` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci";
mysql --user=${ROOT_MYSQL_USER} --password=${ROOT_MYSQL_PASS} -e "CREATE USER '$SYMFONY__DATABASE__USER'@'0.0.0.0' IDENTIFIED BY '$SYMFONY__DATABASE__PASS';"
mysql --user=${ROOT_MYSQL_USER} --password=${ROOT_MYSQL_PASS} -e "GRANT ALL ON $SYMFONY__DATABASE__NAME.* TO '$SYMFONY__DATABASE__USER'@'0.0.0.0' IDENTIFIED BY '$SYMFONY__DATABASE__PASS' WITH GRANT OPTION;"
mysql --user=${ROOT_MYSQL_USER} --password=${ROOT_MYSQL_PASS} -e "GRANT ALL ON $SYMFONY__DATABASE__NAME.* TO '$SYMFONY__DATABASE__USER'@'%' IDENTIFIED BY '$SYMFONY__DATABASE__PASS' WITH GRANT OPTION;"
mysql --user=${ROOT_MYSQL_USER} --password=${ROOT_MYSQL_PASS} -e "FLUSH PRIVILEGES;"

echo "# Restarting MariaDB"
service mysql restart

echo "# Restarting PHP-FPM"
service php7.1-fpm restart

# Below commands should be run from application root dir
cd $DATA_WWW

echo "# Generating and installing assets"
npm install
npm install -g grunt grunt-cli
grunt just-build

echo "# Installing composer dependencies"
runuser -l vagrant -c "cd $DATA_WWW && composer install"

echo "# Creating database schema"
php bin/console doctrine:schema:create

echo "# Creating fixtures"
php bin/console doctrine:fixtures:load

echo "# Setting up remote scripts"
chmod +x symfony.sh
chmod +x composer.sh

echo "# Finished configuring software and application"

IP_ADDRESS=`ifconfig enp0s8 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1 }'`
echo "# Your application should be available on http://$IP_ADDRESS"
